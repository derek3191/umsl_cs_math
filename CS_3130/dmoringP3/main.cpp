/**************************/
/*  Derek Moring CS3130   */
/*        Project 3       */ 
/*         main.cpp       */
/**************************/
#include <iostream>
#include <ctime>
#include <cctype>
#include <string>
#include <fstream>
#include "buildTree.h"
#include "traversals.h"

using namespace std;
int main(int argc, char *argv[]){
	string incomingText; 
	int key = 0;
	string filename;
	char choice;
	
	
	//Handle arguments
	// If they give a file name, make sure you can 
	// Open it and start reading integers and add a
	// new node.
	
	//They gave a file name
	if (argc == 2){
		filename = argv[1];
		ifstream dataFile (filename.c_str());
		if(dataFile.is_open()){
			while(dataFile >> key){
				addLeaf(key);
			}
			dataFile.close();
		}
		else {
			cout << "Exiting due to input file not found\n";
			cout << "tree [filename]\n";
			return 1;
		}
	}

	// Only executable given
	else if (argc == 1) {
		cout << "Enter Some Numbers [press Enter and ctrl+d when finished]: ";
		while (cin >> key){
			addLeaf(key);
		}
	}

	//Too many arguments
	else {
		cout << "Your input is not valid. Please try the following format:\n";
		cout << "tree [filename]\n";
		return 0;
	}

	// Display Menu 
	cin.clear(); // Clear the buffer to prepare for the choice input
	while(1){
	node *root = getRoot();
	
		cout << "Pick a Traversal or Select q to quit\n";
		cout << "In Order: (1)\n";
		cout << "Post Order: (2)\n";
		cout << "Pre Order: (3)\n";
		cout << "Level Order: (4)\n";

		cin >> choice;

		switch(choice){
			case '1':
					system("clear");
					cout << "InOrder Traversal:\n";	
					inOrder(root);
					break;
			case '2': 
					system("clear");
					cout << "PostOrder Traversal:\n";
					postOrder(root);
					break;
			case '3': 
					system("clear");
					cout << "PreOrder Traversal:\n";
					preOrder(root);
					break;
			case '4': 
					system("clear");
					cout << "LevelOrder Traversal:\n";
					levelOrder(root);
					break;
			case 'q':	
						exit(1);
						break;
			case 'Q': 	
						exit(1);
						break;	
		}

	}
	return 0;
}

