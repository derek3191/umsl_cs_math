/**************************/
/*  Derek Moring CS3130   */
/*        Project 3       */ 
/*      buildTree.cpp     */
/**************************/
#include "buildTree.h"
#include "node.h"
#include <iostream>
#include <string>

using namespace std;
node *root = NULL;


// CreateLeaf is a function that returns a new node
// it is called from with the addLeafPrivate function
node *createLeaf(int key, int level){
	node *n = new node;
	n->left = NULL;
	n->right = NULL;
	n->parent = NULL;
	n->key = key;
	n->level = level;
	return n;	
}

// addLeaf is called from the main function only so
// only the buildTree file can handle the root to add
// to portability.
void addLeaf(int key){
	addLeafPrivate(key, root);
}

// addLeafPrivate enforces the rules of the BST
// to only add smaller values to the left and larger
// values to the right.
void addLeafPrivate(int key, node *ptr){
	
	if (root == NULL){ // The tree is empty, create a new root
		root = createLeaf(key, 0);
		root->parent = root;
	}
	else if (key <= ptr->key){ // The incoming number is <= to the key
		if (ptr->left == NULL){ // Add a new node
			ptr->left = createLeaf(key, ((ptr->level)+1));
			ptr->left->parent = ptr; 
		}
		else   					// Or keep looking left
			addLeafPrivate(key, ptr->left);
	}	
	else {
		if(ptr->right == NULL){ // Add a new node
			ptr->right = createLeaf(key, ((ptr->level)+1));
			ptr->right->parent = ptr;
		}
		else if (key == ptr->right->key){ // Add a new node if it equals
			ptr->left = createLeaf(key, ((ptr->level)+1));
		}
		else
			addLeafPrivate(key, ptr->right); // Keep looking right
	}
}

node *getRoot(){
	return root; 
}

