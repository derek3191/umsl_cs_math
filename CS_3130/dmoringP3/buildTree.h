/**************************/
/*  Derek Moring CS3130   */
/*       Project 3        */ 
/*      buildTree.h       */
/**************************/
#include "node.h"

node *createLeaf(node key, int level);
void addLeaf(int key);
void addLeafPrivate(int key, node *ptr);
node *getRoot();
