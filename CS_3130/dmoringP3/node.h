/**************************/
/*  Derek Moring CS3130   */
/*        Project 3       */ 
/*         node.h         */
/**************************/
#ifndef NODE_H
#define NODE_H

using namespace std;

struct node {
	int key;
	node *left;
	node *right;
	node *parent;
	int level;
};

#endif
