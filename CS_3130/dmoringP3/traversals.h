/**************************/
/*  Derek Moring CS3130   */
/*      Project 3         */ 
/*     traversals.h       */
/**************************/
#include "node.h"
void inOrder(node *n);
void preOrder(node *n);
void postOrder(node *n);
void levelOrder(node *n);

