/**************************/
/*  Derek Moring CS3130   */
/*        Project 3       */ 
/*     traversals.cpp     */
/**************************/
#include "traversals.h"
#include <iostream>
#include <queue>

int currentLevel = 0;
void inOrder(node *root){
	if(root != NULL){
		inOrder(root->left);
		for (int i = 0; i < root->level; ++i) { // Add a space for each level
			cout << " ";
		}
		cout << root->key << endl;
		inOrder(root->right);
	}
}

void preOrder(node *root){
	if(root != NULL){
		for (int i = 0; i < root->level; ++i){// Add a space for each level
			cout << " ";
		}
		cout << root->key << endl;
		preOrder(root->left);
		preOrder(root->right);
	}
}

void postOrder(node *root){
	if(root != NULL){
		postOrder(root->left);
		postOrder(root->right);
		for (int i = 0; i < root->level; ++i) {// Add a space for each level
			cout << " ";
		}
		cout << root->key << endl;
	}
}

void levelOrder(node *root){
	queue<node *> q;

	q.push(root);

	while (!q.empty()){
		node *n = q.front();
		cout << n->key << " ";

		if (n->left != NULL)
			q.push(n->left);

		if (n->right != NULL)
			q.push(n->right);

		cout << endl;
		q.pop();
	}
}
