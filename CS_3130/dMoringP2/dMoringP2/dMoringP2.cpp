/*************/
/* dMoringP2 */
/*************/

#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int mergeNumComparisons, mergeNumSwaps = 0;

void merge(int array[], int leftIndex, int middleIndex, int rightIndex){
	int i, j, k, newArray[rightIndex];
	i = k = leftIndex;
	j = middleIndex + 1;
	while ( i <= middleIndex && j <= rightIndex){
		if (array[i] < array[j])
		{
			newArray[k] = array[i];
			k++;
			i++;
		}
		else {
			newArray[k] = array[j];
			k++;
			j++;
		}
		mergeNumComparisons++;
	}

	while (i <= middleIndex) {
		newArray[k] = array[i];
		k++;
		i++;
		mergeNumSwaps++;
	}

	while (j <= rightIndex){
		newArray[k] = array[j];
		k++;
		j++;
		mergeNumSwaps++;
	}

	for (i = leftIndex; i < k; ++i)
	{
		array[i] = newArray[i];
	}

	return;
}

void mergeSort(int array[], int leftIndex, int rightIndex){
	if (leftIndex < rightIndex){
		int middleIndex = (leftIndex + rightIndex) / 2;
		mergeSort(array, leftIndex, middleIndex);
		mergeSort(array, middleIndex+1, rightIndex);
		merge(array, leftIndex, middleIndex, rightIndex);
	}
	return;
}

void insertionSort(int array[], int size){
	int insertionNumSwaps = 0, insertionNumComparisons = 0;
	int j, key, temp;
	for (int i = 1; i < size; i++)
	{
		key = array[i];
		j = i -1;
		insertionNumComparisons++;
		while (j >= 0 && array[j] > key){
			insertionNumComparisons++;
			insertionNumSwaps++;
			array[j+1] = array[j];
			j--;
		}
		array[j+1] = key;

	}
	cout << "Insertion: " << insertionNumComparisons << ", " << insertionNumSwaps << endl;
	insertionNumComparisons = insertionNumSwaps = 0;
}

int main(){
	static int ARRAY_SIZE = 1000;


	int mergeArray[ARRAY_SIZE];
	int insertionArray[ARRAY_SIZE];
	srand(time(NULL)); //Init the random seed

	// Iterate 1000 times for 1000 different random arrays to sort
	for (int i = 0; i < 1000; i++)
	{
		mergeNumSwaps = mergeNumComparisons = 0;
		// Generate random array
		for (int j = 0; j < ARRAY_SIZE; j++)
		{
			insertionArray[j] = mergeArray[j] = rand() % 1000001;
		}

		insertionSort(insertionArray, ARRAY_SIZE);
		mergeSort(mergeArray, 0, ARRAY_SIZE);
		cout << "Merge: " << mergeNumComparisons << ", " << mergeNumSwaps << endl;

	}
}

