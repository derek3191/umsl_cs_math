/******************/
/*  Derek Moring  */
/*   CS3130 P1    */
/******************/
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <fstream>
#include <vector>

using namespace std;
int main(int argc, char *argv[]) {
	static int ARRAY_SIZE = 1000;
	int numSwaps = 0, numComparisons = 0;
	int numArray[ARRAY_SIZE];
	bool BREAK_FLAG = false;
	srand(time(NULL)); //Init the random seed


	cout << "Iterations   Comparisons   Swaps\n";
	cout << "--------------------------------\n";
	//Loop for 1000 iterations
	for (int k = 0; k < 1000; k++) {
		numSwaps = numComparisons = 0;

		//Generate a new number between 0 to 1,000,000 and apply to array
		for (int i = 0; i < ARRAY_SIZE; i++) {
			numArray[i] = rand() % 1000001; 
		}
	
		//Implement the sort
		for (int i = 0; i < ARRAY_SIZE; i++) {
			BREAK_FLAG = false;
			for (int j = 0; j < ARRAY_SIZE-1; j++) {
				numComparisons++;
				if (numArray[j] > numArray[j+1]){
					BREAK_FLAG = true;
					numSwaps++;
					int temp = numArray[j];
					numArray[j] = numArray[j+1];
					numArray[j+1] = temp;
				}
			}
			if (BREAK_FLAG == false){
				break;
			}
		}
		cout << k+1 << ", " << numComparisons << ", " << numSwaps << endl;
	}
	return 0;
}
