function loadFDRText(){
	var xmlhttp;
	if (window.XMLHttpRequest){
		xmlhttp=new XMLHttpRequest();
	}
	else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("speech").innerHTML=xmlhttp.responseText;
			document.getElementById("jfk").checked=false;
		}
	}

	xmlhttp.open("GET", "fdr.txt", true);
	xmlhttp.send();
}

function loadJFKText(){
	var xmlhttp;
	if (window.XMLHttpRequest){
		xmlhttp=new XMLHttpRequest();
	}
	else {
		xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	}

	xmlhttp.onreadystatechange=function(){
		if (xmlhttp.readyState==4 && xmlhttp.status==200){
			document.getElementById("speech").innerHTML=xmlhttp.responseText;
			document.getElementById("fdr").checked=false;
		}
	}

	xmlhttp.open("GET", "jfk.txt", true);
	xmlhttp.send();
}
