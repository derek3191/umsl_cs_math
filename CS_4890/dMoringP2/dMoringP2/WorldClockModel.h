//
//  WorldClockModel.h
//  dMoringP2
//
//  Created by Derek Moring on 10/16/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TimezoneLocations.h"

#pragma mark - Protocol - worldClock Delegate
@protocol WorldClockDelegate <NSObject>

-(void) worldClockHasBeenUpdatedWithNewMinute:(NSInteger) newMinute;

@end

#pragma mark - Public Declarations
@interface WorldClockModel : NSObject
@property (nonatomic, strong) NSMutableArray *timeZonesChosen;
@property (nonatomic, strong) NSMutableArray *timeZonesKnown;


@property (nonatomic, weak) id<WorldClockDelegate> delegate;

-(void) keepTrackOfSeconds;
+(id)sharedWorldClock;
-(TimezoneLocations *) getTimeForTimeZoneAbbreviation:(NSString *) abbreviation;
@end
