//
//  StopwatchModel.m
//  dMoringP2
//
//  Created by Derek Moring on 10/16/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "StopwatchModel.h"

#pragma mark - Private Declarations
@interface StopwatchModel()
@property (nonatomic, strong) NSTimer *stopwatchTimer;
@property (nonatomic, strong) NSTimer *lapTimer;

@property (nonatomic) int currentPrivateHundredthStopwatchTime;
@property (nonatomic) int currentPrivateSecondStopwatchTime;
@property (nonatomic) int currentPrivateMinuteStopwatchTime;
@property (nonatomic) int currentPrivateHourStopwatchTime;

@property (nonatomic) int currentLapPrivateHundredthStopwatchTime;
@property (nonatomic) int currentLapPrivateSecondStopwatchTime;
@property (nonatomic) int currentLapPrivateMinuteStopwatchTime;
@property (nonatomic) int currentLapPrivateHourStopwatchTime;

@property (nonatomic) int numberOfLapsForStopwatch;
@property (nonatomic, strong) NSString *formattedStopwatchStringValue;
@property (nonatomic, strong) NSString *formattedCurrentLapStringValue;

@end

#pragma mark - Implementation
@implementation StopwatchModel
-(id) init{
    self = [super init];
    if (self) {
        _currentPrivateHundredthStopwatchTime = 0;
        _currentPrivateSecondStopwatchTime = 0;
        _currentPrivateMinuteStopwatchTime = 0;
        _currentPrivateHourStopwatchTime = 0;
        _numberOfLapsForStopwatch = 0;
        _currentLapPrivateHundredthStopwatchTime = 0;
        _currentLapPrivateSecondStopwatchTime = 0;
        _currentLapPrivateMinuteStopwatchTime = 0;
        _currentLapPrivateHourStopwatchTime = 0;
        
    }
    return self;
}

#pragma mark - Main Stopwatch Methods

-(void)startMainTimer{
    if (!_isPaused) {
        _lapValuesArray = [NSMutableArray array];
    }
    _isPaused = NO;
    _stopwatchTimer = [NSTimer scheduledTimerWithTimeInterval:1.0/100.0 target:self selector:@selector(updateMainTimer) userInfo:nil repeats:YES];
    _lapTimer = [NSTimer scheduledTimerWithTimeInterval:1.0/100.0 target:self selector:@selector(updateLapTimer) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_stopwatchTimer forMode:NSRunLoopCommonModes];

    [[NSRunLoop mainRunLoop] addTimer:_lapTimer forMode:NSRunLoopCommonModes];

}

-(void)updateMainTimer{
    //NSLog(@"SW: %d %d", _currentPrivateSecondStopwatchTime, )
    if (_currentPrivateHundredthStopwatchTime < 99) {
        _currentPrivateHundredthStopwatchTime++;
    }
    else {
        _currentPrivateHundredthStopwatchTime = 0;
        if (_currentPrivateSecondStopwatchTime < 59) {
            _currentPrivateSecondStopwatchTime++;
        }
        else {
            _currentPrivateSecondStopwatchTime = 0;
            if (_currentPrivateMinuteStopwatchTime < 59) {
                _currentPrivateMinuteStopwatchTime++;
            }
            else {
                _currentPrivateMinuteStopwatchTime = 0;
                if (_currentPrivateHourStopwatchTime < 23) {
                    _currentPrivateHourStopwatchTime++;
                }
                else {
                    _currentPrivateHourStopwatchTime = 0;
                }
            }
        }
    }
    _formattedStopwatchStringValue = [NSString stringWithFormat:@"%02d:%02d:%02d.%02d",_currentPrivateHourStopwatchTime,_currentPrivateMinuteStopwatchTime, _currentPrivateSecondStopwatchTime,_currentPrivateHundredthStopwatchTime];
    
    [self.delegate stopwatchHasBeenUpdatedWithString:_formattedStopwatchStringValue];
}


#pragma mark - Lap Timer Methods
-(void)startLapTimer{
    [_lapTimer invalidate];
    _currentLapPrivateHundredthStopwatchTime = 0;
    _currentLapPrivateSecondStopwatchTime = 0;
    _currentLapPrivateMinuteStopwatchTime = 0;
    _currentLapPrivateHourStopwatchTime = 0;
    _lapTimer = [NSTimer scheduledTimerWithTimeInterval:1.0/100.0 target:self selector:@selector(updateLapTimer) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_lapTimer forMode:NSRunLoopCommonModes];

    
    
    [_lapValuesArray addObject:_formattedCurrentLapStringValue];
}
-(void)updateLapTimer{
    if (_currentLapPrivateHundredthStopwatchTime < 99) {
        _currentLapPrivateHundredthStopwatchTime++;
    }
    else {
        _currentLapPrivateHundredthStopwatchTime = 0;
        if (_currentLapPrivateSecondStopwatchTime < 59) {
            _currentLapPrivateSecondStopwatchTime++;
        }
        else {
            _currentLapPrivateSecondStopwatchTime = 0;
            if (_currentLapPrivateMinuteStopwatchTime < 59) {
                _currentLapPrivateMinuteStopwatchTime++;
            }
            else {
                _currentLapPrivateMinuteStopwatchTime = 0;
                if (_currentLapPrivateHourStopwatchTime < 23) {
                    _currentLapPrivateHourStopwatchTime++;
                }
                else {
                    _currentLapPrivateHourStopwatchTime = 0;
                }
            }
        }
    }
    _formattedCurrentLapStringValue = [NSString stringWithFormat:@"%02d:%02d:%02d.%02d", _currentLapPrivateHourStopwatchTime, _currentLapPrivateMinuteStopwatchTime, _currentLapPrivateSecondStopwatchTime, _currentLapPrivateHundredthStopwatchTime];
    [self.delegate currentLapTimeHasBeenUpdatedWithString:_formattedCurrentLapStringValue];
}

#pragma mark - General Timer Methods
-(void)pauseStopwatch{
    [_stopwatchTimer invalidate];
    _stopwatchTimer = nil;
    [_lapTimer invalidate];
    _lapTimer = nil;
    _isPaused = YES;
    
}
-(void)resetStopwatch{
    _currentPrivateHundredthStopwatchTime = 0;
    _currentPrivateSecondStopwatchTime = 0;
    _currentPrivateMinuteStopwatchTime = 0;
    _currentPrivateHourStopwatchTime = 0;
    _numberOfLapsForStopwatch = 0;
    _currentLapPrivateHundredthStopwatchTime = 0;
    _currentLapPrivateSecondStopwatchTime = 0;
    _currentLapPrivateMinuteStopwatchTime = 0;
    _currentLapPrivateHourStopwatchTime = 0;
    _lapValuesArray = nil;
    _isPaused = NO;
}
-(void)resumeStopwatch{
    [self startMainTimer];
}

@end

