//
//  CoreDataHelper.m
//  dMoringP2
//
//  Created by Derek Moring on 10/17/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "CoreDataHelper.h"
#import "CoreDataManager.h"
#import "StopwatchModel.h"

@interface CoreDataHelper()
@property (nonatomic) StopwatchModel *stopwatchModel;

@end
@implementation CoreDataHelper
- (void)insertStopwatchArray:(NSArray *)stopwatchArray forPredicate:(NSString *)predicate{
    NSManagedObjectContext *context = [[CoreDataManager sharedManager] managedObjectContext];
    NSError *error;

    NSData *lapValueData = [NSKeyedArchiver archivedDataWithRootObject:[self.stopwatchModel lapValuesArray]];
    NSManagedObject *stopwatchObject = [NSEntityDescription insertNewObjectForEntityForName:@"Stopwatch" inManagedObjectContext:context];
    
    [stopwatchObject setValue:lapValueData forKey:@"sLapValuesArray"];
    //[stopwatchObject setValue:[self.stopwatchModel ] forKey:<#(NSString *)#>]
    
    [context save:&error];
}

@end
