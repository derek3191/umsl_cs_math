//
//  TimezoneLocations.h
//  dMoringP2
//
//  Created by Derek Moring on 10/19/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimezoneLocations : NSObject
@property (nonatomic, strong) NSString *timeZoneName;
@property (nonatomic, strong) NSString *currentTime;
@property (nonatomic, strong) NSString *dayComparedToCurrentDay;
@property (nonatomic) NSInteger hour;
@property (nonatomic) NSInteger minute;
@property (nonatomic) NSInteger day;
@property (nonatomic) NSInteger month;
@property (nonatomic) NSInteger year;

-(NSString *) formatCurrentTimeWithHour:(NSInteger)hour andMinute:(NSInteger)minute;
-(NSMutableString *) formatDetailedInformationWithDay:(NSInteger)day andHour:(NSInteger)hour;
@end
