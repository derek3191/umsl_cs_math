//
//  WorldClockModel.m
//  dMoringP2
//
//  Created by Derek Moring on 10/16/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "WorldClockModel.h"
#import "CoreDataManager.h"
#import "TimezoneLocations.h"

#pragma mark - Private Declaration
@interface WorldClockModel()

@property (nonatomic, strong) NSTimer *secondsTimer;
@property (nonatomic) int currentSeconds;

@end

#pragma mark - Implementation
@implementation WorldClockModel

+(id)sharedWorldClock{
    static WorldClockModel *worldclock = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        worldclock = [[WorldClockModel alloc]init];
        worldclock.timeZonesKnown = [[NSTimeZone knownTimeZoneNames] mutableCopy];
        worldclock.timeZonesChosen = [NSMutableArray array];
        
    });
    return worldclock;
    
}

#pragma mark - Getting Timezone Data
- (TimezoneLocations *)getTimeForTimeZoneAbbreviation:(NSString *)timeZoneName {
    
    TimezoneLocations *tz = [[TimezoneLocations alloc]init];
    
    NSDate *currentDate = [NSDate date];
    NSTimeZone *localZone = [NSTimeZone localTimeZone];
    NSTimeZone *diffZone = [NSTimeZone timeZoneWithName:timeZoneName];
    NSInteger localOffset = [localZone secondsFromGMTForDate:currentDate];
    NSInteger diffOffset = [diffZone secondsFromGMTForDate:currentDate];
    NSTimeInterval interval = diffOffset - localOffset;
    NSDate *diffDate = [NSDate dateWithTimeInterval:interval sinceDate:currentDate];
    NSDateFormatter *format = [[NSDateFormatter alloc]init];
    [format setDateFormat:@"MM/dd/yyyy h:mm"];
    NSDateComponents *comp = [[NSCalendar currentCalendar] components:NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit|NSDayCalendarUnit|NSMonthCalendarUnit|NSYearCalendarUnit fromDate:diffDate];
    tz.currentTime = [NSString stringWithFormat:@"%02d:%02d",[comp hour], [comp minute]];
    tz.hour = [comp hour];
    tz.minute = [comp minute];
    tz.day = [comp day];
    tz.month = [comp month];
    tz.year = [comp year];
    tz.timeZoneName = timeZoneName;
    
    return tz;
}

#pragma mark - Timer Methods
- (void) keepTrackOfSeconds{
    NSDate *now = [[NSDate alloc]init];
    NSDateComponents *comp = [[NSCalendar currentCalendar] components:NSSecondCalendarUnit fromDate:now];
    _currentSeconds = [comp second];
    
    _secondsTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateSeconds) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_secondsTimer forMode:NSRunLoopCommonModes];
}

- (void) updateSeconds{
    NSLog(@"%i", _currentSeconds);
    if (_currentSeconds < 59) {
        _currentSeconds++;
    }
    else {
        _currentSeconds = 0;
    }
    
    if (_currentSeconds == 0) {
        NSDate *now = [NSDate date];
        NSDateComponents *comp = [[NSCalendar currentCalendar] components:NSMinuteCalendarUnit fromDate:now];
        

            [self.delegate worldClockHasBeenUpdatedWithNewMinute:[comp minute]];
    }
}

@end