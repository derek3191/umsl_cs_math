//
//  WorldClockViewController.m
//  dMoringP2
//
//  Created by Derek Moring on 10/16/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "WorldClockViewController.h"
#import "WorldClockModel.h"
#import "WorldClockTableViewCell.h"
#import "TimezoneLocations.h"

#pragma mark - Private Declarations
@interface WorldClockViewController () <WorldClockDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableViewChosen;
@property (weak, nonatomic) IBOutlet UITableView *tableViewKnown;
@property (nonatomic, strong) WorldClockModel *model;

@end

#pragma mark - Implementation
@implementation WorldClockViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.model = [WorldClockModel sharedWorldClock];
    [self.model setDelegate:self];
    [self.model keepTrackOfSeconds];

}

- (void)viewDidAppear:(BOOL)animated{
    [self.tableViewKnown setHidden:YES];
    [self.tableViewChosen setHidden:NO];
    [self.tableViewChosen reloadData];
    [self.tableViewKnown reloadData];
}

#pragma mark - Table View Datasource Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.tableViewChosen) {
        return [[self.model timeZonesChosen]count];
    }
    else if (tableView == self.tableViewKnown){
        return [[self.model timeZonesKnown]count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    WorldClockTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"worldClockCell"];
    if (tableView == self.tableViewChosen) {
        TimezoneLocations *tz = [self.model.timeZonesChosen objectAtIndex:indexPath.row];
        cell.chosenZoneName.text = tz.timeZoneName;
        cell.chosenDate.text = [tz formatDetailedInformationWithDay:tz.day andHour:tz.hour];
        cell.digitalClockOutlet.text = [tz formatCurrentTimeWithHour:tz.hour andMinute:tz.minute];
    }
    else if (tableView == self.tableViewKnown){
        NSString *zoneName = [self.model.timeZonesKnown objectAtIndex:indexPath.row];
        cell.knownZoneName.text = zoneName;
        return cell;
    }
    return cell;
  
}

#pragma mark - table View Delegate Methods
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.tableViewKnown) {
        NSString *abbr;
        NSTimeZone *zoneName;
        zoneName = [NSTimeZone timeZoneWithName:[self.model.timeZonesKnown objectAtIndex:indexPath.row]];
        abbr = [self.model.timeZonesKnown objectAtIndex:indexPath.row];
        TimezoneLocations *tz = [self.model getTimeForTimeZoneAbbreviation:abbr];
        [[self.model timeZonesChosen] addObject:tz];
        
        [[self.model timeZonesKnown] removeObjectAtIndex:indexPath.row];
        
        
        [self.tableViewKnown reloadData];
        [self.tableViewChosen reloadData];
        [self.tableViewChosen setHidden:NO];
        [self.tableViewKnown setHidden:YES];
        [self.addTimeZoneOutlet setEnabled:YES];
        [self.editTimeZoneOutlet setTitle:@"Edit"];
    }
}



#pragma mark - Buttons
- (IBAction)editTimeZoneButton:(id)sender {
    //In edit mode... taking it out of edit mode
    if (![self.tableViewKnown isHidden]) {
        [self.tableViewChosen setHidden:NO];
        [self.tableViewKnown setHidden:YES];
        [self.editTimeZoneOutlet setTitle:@"Edit"];
        [self.addTimeZoneOutlet setEnabled:YES];
    }
    else {
        if ([self.tableViewChosen isEditing]) {
            [self.addTimeZoneOutlet setEnabled:YES];
            [self.tableViewChosen setEditing:NO animated:YES];
            [self.editTimeZoneOutlet setTitle:@"Edit"];
            [self.tableViewChosen endUpdates];
        
        }
        else {
            [self.addTimeZoneOutlet setEnabled:NO];
            [self.editTimeZoneOutlet setTitle:@"Done"];
            [self.tableViewChosen setEditing:YES animated:YES];
            [self.tableViewChosen beginUpdates];
        }
    }
}

- (IBAction)addTimeZoneButton:(id)sender {
    // if is editing, dont do anything
    // else, tKnown hid NO. tChose hid YES
    // make editTimeZoneOUtlet setTitle:@"cancel"
    // cancel will swap the hidden tables
    
    if (![self.tableViewChosen isEditing]) {
        [self.addTimeZoneOutlet setEnabled:NO];
        [self.editTimeZoneOutlet setTitle:@"Cancel"];
    }
    
    [self.tableViewKnown setHidden:NO];
    [self.tableViewChosen setHidden:YES];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (editingStyle == UITableViewCellEditingStyleDelete){
        NSLog(@"row: %i",indexPath.row);
       
        //[self.model.timeZonesKnown addObject:[self.model.timeZonesChosen objectAtIndex:indexPath.row]];
        //[self.model.timeZonesKnown sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        [self.model.timeZonesChosen removeObjectAtIndex:indexPath.row];
        
        [self.tableViewChosen deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
        
    }
}

#pragma mark - worldClockDelegate Methods
- (void) worldClockHasBeenUpdatedWithNewMinute:(NSInteger)newMinute{
    for (TimezoneLocations *it in [self.model timeZonesChosen]) {
        if (newMinute < 59) {
            it.minute++;
        }
        else {
            it.minute = 0;
            if (it.hour < 23) {
                it.hour++;
            }
            else {
                it.hour = 0;
            }
        }
    }
    [self.tableViewChosen reloadData];
}
@end
