//
//  TimerViewController.m
//  dMoringP2
//
//  Created by Derek Moring on 10/16/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "TimerViewController.h"
#import "TimerModel.h"

#pragma mark - Private Declarations
@interface TimerViewController () <UIPickerViewDataSource, UIPickerViewDelegate, timerDelegate>

@property (nonatomic, weak) IBOutlet UIPickerView *pickerView;
@property (nonatomic, strong) TimerModel *model;

@end

#pragma mark - Implementation

@implementation TimerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.model = [[TimerModel alloc] init];
    [self.model setDelegate:self];
    
    if ([self.pickerView selectedRowInComponent:1] == 0) {
        [self.pickerView selectRow:1 inComponent:1 animated:YES];
    }
    
    [self.timeRemainingOutlet setHidden:YES];
    [self.pickerView setHidden:NO];
    // Do any additional setup after loading the view.
}

#pragma mark - DataSource


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        return [[self.model hoursArray] count];
    }
    else
        return [[self.model minutesArray] count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0) {
        return [NSString stringWithFormat:@"%@", [self.model.hoursArray objectAtIndex:row]];
    } else {
        return [NSString stringWithFormat:@"%@", [self.model.minutesArray objectAtIndex:row]];
    }
}


#pragma mark - button Methods

- (IBAction)startButtonPressed:(id)sender {
    NSInteger hoursIndex, minutesIndex;
    
    // Set label to color black
    self.timeRemainingOutlet.textColor = [UIColor blackColor];
    [self.timeRemainingOutlet setHidden:NO];
    [self.pickerView setHidden:YES];
    
    //CHANGE BUTTONS HIDDEN
    [self.resetButtonOutlet setHidden:NO];
    [self.resumeButtonOutlet setHidden:YES];
    [self.pauseButtonOutlet setEnabled:YES];
    [self.startButtonOutlet setHidden:YES];
    
    hoursIndex = [self.pickerView selectedRowInComponent:0];
    minutesIndex = [self.pickerView selectedRowInComponent:1];

    self.model.startHours = [self.model.hoursArray objectAtIndex:hoursIndex];
    self.model.startMinutes = [self.model.minutesArray objectAtIndex:minutesIndex];
    self.model.isRunning = YES;
    [self.model startTimerWithSeconds:0];
    
}

- (IBAction)pausedButtonPressed:(id)sender {
    
    // HIDE NECESSARY BUTTONS
    [self.pauseButtonOutlet setHidden:YES];
    [self.resumeButtonOutlet setHidden:NO];
    self.model.isRunning = NO;
    [self.model pauseTimer];

}

- (IBAction)cancelButtonPressed:(id)sender {
    NSInteger resetHourIndex, resetMinuteIndex;
    self.model.isRunning = NO;
    [self.model cancelTimer];
    
    // hide buttons
    [self.startButtonOutlet setHidden:NO];
    [self.pauseButtonOutlet setEnabled:NO];
    [self.pauseButtonOutlet setHidden:NO];
    [self.resumeButtonOutlet setHidden:YES];
    [self.resetButtonOutlet setHidden:YES];
    
    resetHourIndex = [self.model.hoursArray indexOfObject:self.model.startHours];
    resetMinuteIndex = [self.model.minutesArray indexOfObject:self.model.startMinutes];
    // Reset components to previous starting time
    [self.pickerView selectRow:resetHourIndex inComponent:0 animated:YES];
    [self.pickerView selectRow:resetMinuteIndex inComponent:1 animated:YES];
    // hide the label
    [self.pickerView setHidden:NO];
    [self.timeRemainingOutlet setHidden:YES];
    
    

}

- (IBAction)resumeButtonPressed:(id)sender {
    self.model.isRunning = YES;
    [self.model resumeTimer];
    
    [self.pauseButtonOutlet setHidden:NO];
    [self.resumeButtonOutlet setHidden:YES];
}

#pragma mark - Timer Delegate Method
- (void)timerHasBeenUpdatedWithString:(NSString *)timerString{
    if (self.model.hasEnded) {
        self.timeRemainingOutlet.text = [NSString stringWithFormat:@"- %@",timerString];
        self.timeRemainingOutlet.textColor = [UIColor redColor];
    }
    else {
        self.timeRemainingOutlet.text = timerString;

    }
}

@end
