//
//  TimerViewController.h
//  dMoringP2
//
//  Created by Derek Moring on 10/16/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - Public Declaration
@interface TimerViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *timeRemainingOutlet;
@property (weak, nonatomic) IBOutlet UIButton *startButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *pauseButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *resetButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *resumeButtonOutlet;

@end
