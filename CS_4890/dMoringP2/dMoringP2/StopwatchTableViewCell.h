//
//  StopwatchTableViewCell.h
//  dMoringP2
//
//  Created by Derek Moring on 10/16/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StopwatchTableViewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *cellLapNumber;
@property (nonatomic, weak) IBOutlet UILabel *cellLapTime;

@end

