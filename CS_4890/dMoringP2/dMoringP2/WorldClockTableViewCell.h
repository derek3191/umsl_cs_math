//
//  WorldClockTableViewCell.h
//  dMoringP2
//
//  Created by Derek Moring on 10/16/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorldClockTableViewCell : UITableViewCell

#pragma mark - tableViewKnown labels
@property (weak, nonatomic) IBOutlet UILabel *knownZoneName;


#pragma mark - tableViewChosen labels
@property (weak, nonatomic) IBOutlet UILabel *chosenZoneName;
@property (weak, nonatomic) IBOutlet UILabel *chosenDate;
@property (weak, nonatomic) IBOutlet UILabel *digitalClockOutlet;


- (void)drawRect:(CGRect)rect;
@end

