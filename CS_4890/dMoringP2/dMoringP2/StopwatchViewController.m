//
//  StopwatchViewController.m
//  dMoringP2
//
//  Created by Derek Moring on 10/16/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//


#import "StopwatchViewController.h"
#import "StopwatchModel.h"
#import "StopwatchTableViewCell.h"


#pragma mark - Private Declarations
@interface StopwatchViewController () <stopwatchDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) StopwatchModel *model;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) BOOL isRunning;
@property (nonatomic) BOOL isPaused;

@end
#pragma mark - Implementation
@implementation StopwatchViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.model = [[StopwatchModel alloc]init];
    [self.model setDelegate:self];
    
}

#pragma mark - Button Click Methods
- (IBAction)startButtonClicked:(id)sender {
    _isRunning = YES;
    [self.startButtonOutlet setHidden:YES];
    [self.pauseButtonOutlet setHidden:NO];
    [self.lapButtonOutlet setHidden:NO];
    [self.model startMainTimer];
}

- (IBAction)pauseButtonClicked:(id)sender {
    _isRunning = NO;
    [self.resumeButtonOutlet setHidden:NO];
    [self.resetButtonOutlet setHidden:NO];
    [self.pauseButtonOutlet setHidden:YES];
    [self.lapButtonOutlet setHidden:YES];
    [self.model pauseStopwatch];
}

- (IBAction)lapButtonClicked:(id)sender {
    [self.model startLapTimer];
    [self.tableView reloadData];
}
- (IBAction)resumeButtonClicked:(id)sender {
    [self.resumeButtonOutlet setHidden:YES];
    [self.resetButtonOutlet setHidden:YES];
    [self.pauseButtonOutlet setHidden:NO];
    [self.lapButtonOutlet setHidden:NO];
    [self.model resumeStopwatch];
}
-(IBAction)resetButtonClicked:(id)sender {
    [self.resumeButtonOutlet setHidden:YES];
    [self.resetButtonOutlet setHidden:YES];
    [self.pauseButtonOutlet setHidden:YES];
    [self.lapButtonOutlet setHidden:YES];
    [self.startButtonOutlet setHidden:NO];
    [self.model resetStopwatch];
    [self.tableView reloadData];
    self.clockLabelOutlet.text = @"00:00:00.00";
    self.lapClockLabelOutlet.text = @"00:00:00.00";
}

#pragma mark - Stopwatch Delegating Methods
-(void) stopwatchHasBeenUpdatedWithString:(NSString *)stopwatchString{
    self.clockLabelOutlet.text = stopwatchString;
}
-(void)currentLapTimeHasBeenUpdatedWithString:(NSString *)currentLapString{
    self.lapClockLabelOutlet.text = currentLapString;
}
#pragma mark - Table View Datasource Methods
//lapTableCell
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int sizeOfLapValueArray = [self.model.lapValuesArray count];
    return sizeOfLapValueArray;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    StopwatchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"lapTableCell"];
    NSString *lapTime = [self.model.lapValuesArray objectAtIndex:indexPath.row];
    cell.cellLapNumber.text = [NSString stringWithFormat:@"%i", indexPath.row+1];
    cell.cellLapTime.text = lapTime;
    return cell;
}

@end
