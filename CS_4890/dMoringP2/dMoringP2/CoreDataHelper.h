//
//  CoreDataHelper.h
//  dMoringP2
//
//  Created by Derek Moring on 10/17/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataHelper : NSObject
- (void) insertStopwatchValues;
- (void) insertWorldClockValues;
- (void) insertTimerValues;
@end
