//
//  TimerModel.h
//  dMoringP2
//
//  Created by Derek Moring on 10/16/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - Protocol - Stopwatch Delegate
@protocol timerDelegate <NSObject>

-(void) timerHasBeenUpdatedWithString:(NSString *)timerString;

@end

#pragma mark - Public Declaration
@interface TimerModel : NSObject
@property (nonatomic, strong) NSMutableArray *hoursArray;
@property (nonatomic, strong) NSMutableArray *minutesArray;
@property (nonatomic) BOOL isRunning;
@property (nonatomic) BOOL hasEnded;
@property (nonatomic, strong) NSNumber *startHours;
@property (nonatomic, strong) NSNumber *startMinutes;
@property (nonatomic, strong) NSString *timeRemainingString;
@property (nonatomic, weak) id<timerDelegate> delegate;
-(void)startTimerWithSeconds:(int) seconds;
-(void)cancelTimer;
-(void)pauseTimer;
-(void)resumeTimer;

@end
