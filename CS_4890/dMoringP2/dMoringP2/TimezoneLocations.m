//
//  TimezoneLocations.m
//  dMoringP2
//
//  Created by Derek Moring on 10/19/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "TimezoneLocations.h"

@implementation TimezoneLocations
-(NSString *)formatCurrentTimeWithHour:(NSInteger)hour andMinute:(NSInteger)minute{
    if (hour <= 12) {
        if (hour == 0) {
            return [NSString stringWithFormat:@"12:%02d AM", minute];
        }
        else {
            return [NSString stringWithFormat:@"%2d:%02d AM", hour, minute];
        }
    }
    else {
        hour = hour - 12;
        //minute = minute - 12;
        return [NSString stringWithFormat:@"%2d:%02d PM", hour, minute];
    }
}

-(NSString *)formatDetailedInformationWithDay:(NSInteger)day andHour:(NSInteger)hour{
    NSMutableString *detailedString = [[NSMutableString alloc]init];
    NSString *finalDetailedString;
    NSInteger differenceHours;
    
    NSDate *now = [NSDate date];
    NSDateComponents *comp = [[NSCalendar currentCalendar] components:NSDayCalendarUnit|NSHourCalendarUnit fromDate:now];
    if ([comp day] == day) {
        [detailedString appendString:@"Today"];
        if ([comp hour] == hour) {
            ;
        }
        else if ([comp hour] < hour){
            differenceHours = hour - [comp hour];
            if (differenceHours > 1) {
                [detailedString appendString:[NSString stringWithFormat: @", %d hours ahead", differenceHours]];
            } else {
                [detailedString appendString:[NSString stringWithFormat: @", %d hour ahead", differenceHours]];
            }
        }
        else {
            differenceHours = [comp hour] - hour;
            if (differenceHours > 1) {
                [detailedString appendString:[NSString stringWithFormat: @", %d hours behind", differenceHours]];
            }
            else {
                [detailedString appendString:[NSString stringWithFormat: @", %d hour behind", differenceHours]];
            }
        }
    }
    else if ([comp day] < day){
        [detailedString appendString:@"Tomorrow"];
        differenceHours = (24 -[comp hour]) + hour;
        if (differenceHours > 1) {
            [detailedString appendString:[NSString stringWithFormat:@", %d hours ahead",differenceHours]];
        } else {
            [detailedString appendString:[NSString stringWithFormat: @", %d hour ahead", differenceHours]];
        }
    }
    else {
        [detailedString appendString:@"Yesterday"];
        differenceHours = [comp hour] + (24 - hour);
        if (differenceHours > 1) {
            [detailedString appendString:[NSString stringWithFormat:@", %d hours behind",differenceHours]];
        } else {
            [detailedString appendString:[NSString stringWithFormat: @", %d hour behind", differenceHours]];
        }
    }
    
    
    finalDetailedString = [NSString stringWithString:detailedString];
    
    return finalDetailedString;
}
@end
