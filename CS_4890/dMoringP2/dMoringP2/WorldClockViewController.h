//
//  WorldClockViewController.h
//  dMoringP2
//
//  Created by Derek Moring on 10/16/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - Public Declarations

@interface WorldClockViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *editTimeZoneOutlet;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *addTimeZoneOutlet;
- (IBAction)editTimeZoneButton:(id)sender;
- (IBAction)addTimeZoneButton:(id)sender;

@end
