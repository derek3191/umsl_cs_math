//
//  StopwatchViewController.h
//  dMoringP2
//
//  Created by Derek Moring on 10/16/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - Public Declarations
@interface StopwatchViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *clockLabelOutlet;
@property (weak, nonatomic) IBOutlet UILabel *lapClockLabelOutlet;
@property (weak, nonatomic) IBOutlet UIButton *startButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *pauseButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *lapButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *resumeButtonOutlet;
@property (weak, nonatomic) IBOutlet UIButton *resetButtonOutlet;


@end
