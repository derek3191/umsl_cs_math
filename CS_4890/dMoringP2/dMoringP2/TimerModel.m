//
//  TimerModel.m
//  dMoringP2
//
//  Created by Derek Moring on 10/16/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "TimerModel.h"
#import <AudioToolbox/AudioToolbox.h>

#pragma mark - Private Declaration 
@interface TimerModel()
@property (nonatomic, strong) NSTimer *timerTimer;

@property (nonatomic) int currentPrivateSecondTimeRemaining;
@property (nonatomic) int currentPrivateMinuteTimeRemaining;
@property (nonatomic) int currentPrivateHourTimeRemaining;

@property (nonatomic, strong) NSString *formattedStopwatchStringValue;



@end

#pragma mark - Implementation
@implementation TimerModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        _hoursArray = [NSMutableArray array];
        _minutesArray = [NSMutableArray array];
        for (int i = 0; i < 24; i++) {
            NSNumber *val = [NSNumber numberWithInt:i];
            [_hoursArray addObject:val];
        }
        for (int i = 0; i < 60; i++) {
            NSNumber *val = [NSNumber numberWithInt:i];
            [_minutesArray addObject:val];
        }
    }
    return self;
}

#pragma mark - Timer Methods
- (void) startTimerWithSeconds:(int) seconds{
    //_currentPrivateHourTimeRemaining = [_startHours intValue];
    //_currentPrivateMinuteTimeRemaining = [_startMinutes intValue];
    
    _currentPrivateSecondTimeRemaining = seconds;

    _isRunning = YES;
    _hasEnded = NO;
    _timerTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_timerTimer forMode:NSRunLoopCommonModes];

    
}

- (void) updateTimer {
    if (!_hasEnded) {
        if (_currentPrivateSecondTimeRemaining == 0) {
            if (_currentPrivateMinuteTimeRemaining == 0) {
                if (_currentPrivateHourTimeRemaining == 0) {
                    _hasEnded = YES;
                    [self countTimePastEnd];
                    //[self playSound];
                }
                else {
                    _currentPrivateSecondTimeRemaining = 59;
                    _currentPrivateMinuteTimeRemaining = 59;
                    _currentPrivateHourTimeRemaining--;
                }
            }
            else {
                _currentPrivateSecondTimeRemaining = 59;
                _currentPrivateMinuteTimeRemaining--;
            }
        }
        else {
            _currentPrivateSecondTimeRemaining--;
        }
    }
    else {
        [self countTimePastEnd];
    }
    
    _formattedStopwatchStringValue = [NSString stringWithFormat:@"%2d:%02d:%02d", _currentPrivateHourTimeRemaining, _currentPrivateMinuteTimeRemaining, _currentPrivateSecondTimeRemaining];
    
    [self.delegate timerHasBeenUpdatedWithString:_formattedStopwatchStringValue];

}

- (void) countTimePastEnd {
    if (_currentPrivateSecondTimeRemaining < 59) {
        _currentPrivateSecondTimeRemaining++;
    }
    else {
        _currentPrivateSecondTimeRemaining = 0;
        if (_currentPrivateMinuteTimeRemaining < 59) {
            _currentPrivateMinuteTimeRemaining++;
        }
        else {
            _currentPrivateMinuteTimeRemaining = 0;
            if (_currentPrivateHourTimeRemaining < 23) {
                _currentPrivateHourTimeRemaining++;
            }
            else {
                _currentPrivateHourTimeRemaining = 0;
            }
        }
    }
}

- (void)cancelTimer{
    [_timerTimer invalidate];
    _timerTimer = nil;
    _currentPrivateHourTimeRemaining = [_startHours intValue];
    _currentPrivateMinuteTimeRemaining = [_startMinutes intValue];
    _currentPrivateSecondTimeRemaining = 0;
    
    
}
- (void)pauseTimer {
    [_timerTimer invalidate];
    _timerTimer = nil;
}
- (void)resumeTimer {
    [self startTimerWithSeconds:_currentPrivateSecondTimeRemaining];
}
- (void) playSound{
    SystemSoundID sound;
    NSString *path = [[NSBundle mainBundle]pathForResource:@"Beep" ofType:@"aiff"];
    //NSURL *pathURL = [NSURL fileURLWithPath:path];
    
    NSLog(@"\npath: %@", path);
    //AudioServicesCreateSystemSoundID((__bridge CFURLRef)pathURL, &sound);
    //AudioServicesPlayAlertSound(sound);
}

@end
