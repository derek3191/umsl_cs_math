//
//  StopwatchModel.h
//  dMoringP2
//
//  Created by Derek Moring on 10/16/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StopwatchViewController.h"

#pragma mark - Protocol - Stopwatch Delegate
@protocol stopwatchDelegate <NSObject>

-(void) stopwatchHasBeenUpdatedWithString:(NSString *)stopwatchString;
-(void) currentLapTimeHasBeenUpdatedWithString:(NSString *)currentLapString;

@end

#pragma mark - Public Declarations
@interface StopwatchModel : NSObject
@property (nonatomic) BOOL isPaused;
@property (nonatomic, strong) NSString *timeString;
@property (nonatomic) NSMutableArray *lapValuesArray;
@property (nonatomic, weak) id<stopwatchDelegate> delegate;
-(void)startMainTimer;
-(void)startLapTimer;
-(void)pauseStopwatch;
-(void)resetStopwatch;
-(void)resumeStopwatch;
@end

