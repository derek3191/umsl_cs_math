//
//  startTheFireworks.m
//  delegate
//
//  Created by Derek Moring on 9/8/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "startTheFireworks.h"

@interface startTheFireworks()
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) int currentCountdownPrivateValue;
@end

@implementation startTheFireworks

-(id) init{
    self = [super init];
    if (self) {
        self.currentCountdownPrivateValue = 10;
    }
    return self;
}
-(void) startTimer {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(timerHasTicked) userInfo:nil repeats:YES];
}

- (void) timerHasTicked{
    if (self.currentCountdownPrivateValue > 1) {
        self.currentCountdownPrivateValue--;
    }
    else {
        [self.timer invalidate];
        self.currentCountdownPrivateValue = 10;
    }
}
@end
