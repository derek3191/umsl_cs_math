//
//  damViewController.m
//  delegate
//
//  Created by Derek Moring on 9/8/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "damViewController.h"
#import "startTheFireworks.h"

@interface damViewController ()
@property (nonatomic, weak) IBOutlet UIButton *startButonTimer;
@property (nonatomic, weak) IBOutlet UILabel *countdownLabel;
@end

@implementation damViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.model = [[startTheFireworks alloc]init];
    
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.countdownLabel setHidden:YES];
    [self.countdownLabel setText:@"10"];
    
}

-(IBAction)startTheCountdown:(id)sender{
    [self.countdownLabel setHidden:NO];
}


@end
