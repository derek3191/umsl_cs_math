//
//  damAppDelegate.h
//  delegate
//
//  Created by Derek Moring on 9/8/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface damAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
