//
//  startTheFireworks.h
//  delegate
//
//  Created by Derek Moring on 9/8/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FireworksModelDelegate <NSObject>
-(void)currentCountdownNumberUpdatedWithNumber:(NSNumber *)countdownValue;
@end

@interface startTheFireworks : NSObject
-(void)startTimer;

@end
