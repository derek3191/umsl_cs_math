//
//  MenuModel.h
//  DMoringP3
//
//  Created by Derek Moring on 11/5/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MenuModel : NSObject

@property BOOL timeMode;
@property NSInteger gridSizeSegment;
@property NSInteger cardSelectionSegment;


+ (id) sharedManager;
- (void) saveUserDefaults;
- (void) getUserDefaults;
- (void) updateGrid:(NSInteger) grid andCards:(NSInteger) card andTime:(BOOL) time;
@end
