//
//  CardCollectionViewCell.h
//  DMoringP3
//
//  Created by Derek Moring on 11/12/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CardCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *cardImage;
@property (weak, nonatomic) IBOutlet UILabel *cardLabel;
- (void) hideCard;
- (void) showCard;
@end
