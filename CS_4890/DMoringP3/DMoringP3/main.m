//
//  main.m
//  DMoringP3
//
//  Created by Derek Moring on 11/3/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    int retVal = 0;
    @autoreleasepool {
        NSString *classString = NSStringFromClass([AppDelegate class]);
        @try {
            retVal = UIApplicationMain(argc, argv, nil, classString);
        }
        @catch (NSException *exception) {
            NSLog(@"Exception - %@",[exception description]);
            exit(EXIT_FAILURE);
        }
    }
    return retVal;
    
    //@autoreleasepool {
    //   return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    //}
}
