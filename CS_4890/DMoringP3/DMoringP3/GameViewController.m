//
//  ViewController.m
//  DMoringP3
//
//  Created by Derek Moring on 11/3/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "GameViewController.h"
#import "CardCollectionViewCell.h"
#import "GameModel.h"
#import "MenuModel.h"
#import "TimerModel.h"

#pragma mark - Private Declarations
@interface GameViewController () <timerDelegate>

@property (nonatomic, strong) CardCollectionViewCell *cardCell;
@property (nonatomic, strong) GameModel *gameModel;
@property BOOL pairShowing;
@property BOOL gameHasStarted;
@property NSNumber *cardsRemaining;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *timerLabel;
@end

#pragma mark - Implementation
@implementation GameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated{
    [[MenuModel sharedManager] getUserDefaults];
    self.gameModel = [[GameModel alloc]init];
    self.cardCell = [[CardCollectionViewCell alloc]init];
    [[TimerModel sharedManager] setDelegate:self];
    [self.gameModel movePlistsToArrays];
   // [self.collectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Collection View DataSource
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSInteger itemsInSection;
    int cardSelection = [[MenuModel sharedManager] cardSelectionSegment];
    if ([[MenuModel sharedManager] gridSizeSegment] == 0) {
        itemsInSection = 4;
    }
    else if ([[MenuModel sharedManager] gridSizeSegment] == 1){
        itemsInSection = 16;
    }
    else {
        itemsInSection = 36;
        
    }
    [self.gameModel createDeckOfCardsWithFace:cardSelection andDeckSize:itemsInSection/2];
    return itemsInSection;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CardCollectionViewCell *cell = (CardCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    [cell.layer setBorderWidth:3.0];
    [cell.layer setBorderColor:[UIColor whiteColor].CGColor];
    cell.cardLabel.text = [self.gameModel.gameBoardDeck objectAtIndex:indexPath.row];
    cell.cardLabel.textAlignment = NSTextAlignmentCenter;
    [cell.cardImage setImage:[UIImage imageNamed:@"back.png"]];
    cell.cardLabel.hidden = YES;
    return cell;
}


#pragma mark - CollectionView Delegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    int localCardsRemaining;
    static bool firstCardFlipped, secondCardFlipped; // Added static bools to check if the cards are flipped instead of
                                                    // looking at what image is displayed. works in xcode 6
    if (self.cardsRemaining == nil) {
        self.cardsRemaining = [NSNumber numberWithInt:[self.gameModel.gameBoardDeck count]];
        firstCardFlipped = NO;
        secondCardFlipped = NO;
    }
    if (!self.gameHasStarted && [[MenuModel sharedManager] timeMode]) {
        [[TimerModel sharedManager] startTimer];
    }
    self.gameHasStarted = YES;
    static NSIndexPath *firstCardIndexPath;
    CardCollectionViewCell *cell = (CardCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    CardCollectionViewCell *firstCell = (CardCollectionViewCell *)[collectionView cellForItemAtIndexPath:firstCardIndexPath];
    
    // No cards have been selected in the current pair - using the firstCardFlipped bool as new test
    if ([self.gameModel.selectedCards count] == 0 && !self.pairShowing && !firstCardFlipped) {
        firstCardIndexPath = indexPath;
        [cell showCard];
        firstCardFlipped = YES;
        [self.gameModel.selectedCards addObject:[self.gameModel.gameBoardDeck objectAtIndex:indexPath.row]];
    }
    
    // Second card has been selected.. Check for a match - Using the secondCardFlipped as the new test
    else if ([self.gameModel.selectedCards count] == 1 && firstCardIndexPath.row != indexPath.row && !self.pairShowing && !secondCardFlipped){
        [self.gameModel.selectedCards addObject:[self.gameModel.gameBoardDeck objectAtIndex:indexPath.row]];
        [cell showCard];
        secondCardFlipped = YES;
        // Match was found
        if ([self.gameModel.selectedCards objectAtIndex:0] == [self.gameModel.selectedCards objectAtIndex:1]) {
            [self.gameModel.selectedCards removeAllObjects];
            self.gameModel.selectedCards = [NSMutableArray array];
            
            // CHECK IF ANY MORE MATCHES OUT THERE
            localCardsRemaining = self.cardsRemaining.intValue;
            localCardsRemaining -= 2;
            self.cardsRemaining = [NSNumber numberWithInt:localCardsRemaining];
            
            // No More cards.. You Won
            if (self.cardsRemaining.intValue == 0) {
                [[TimerModel sharedManager] cancelTimer];
                [self showGameWonAlert];
                if ([[MenuModel sharedManager] timeMode]) {
                    [self addTimeToArray:self.timerLabel.text];
                }
            }
            firstCardFlipped = NO; // Reset those bools back to false to get the next pair
            secondCardFlipped = NO;
        }
        // Not a match.. try again
        else{
            self.pairShowing = YES;
            // Using Dispatch_after to show the cards for a bit before flipping back
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [cell hideCard];
                [firstCell hideCard];
                firstCardFlipped = NO;
                secondCardFlipped = NO;
                self.pairShowing = NO;
            });
        }
        [self.gameModel.selectedCards removeAllObjects];
    }
    // You pretty much hit the same card twice you cheating fool
    else {
        [self.gameModel.selectedCards removeAllObjects];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    CGSize cellSize;
    
    CardCollectionViewCell *cell = (CardCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    if ([[MenuModel sharedManager] gridSizeSegment] == 0) {
        cellSize = CGSizeMake(100.0, 100.0);
        cell.cardImage.contentMode = UIViewContentModeScaleAspectFit;
    }
    else if ([[MenuModel sharedManager] gridSizeSegment] == 1){
        cellSize = CGSizeMake(60.0, 60.0);
        cell.cardImage.contentMode = UIViewContentModeScaleAspectFit;
    }
    else {
        cellSize = CGSizeMake(35.0, 35.0);
        cell.cardImage.contentMode = UIViewContentModeScaleAspectFit;
    }
    return cellSize;
}

#pragma mark - Button Methods
- (IBAction)resetGameButtonClicked:(id)sender {
    for (CardCollectionViewCell *cell in self.collectionView.visibleCells) {
        [cell hideCard];
        [[TimerModel sharedManager] cancelTimer];
        self.timerLabel.text = @"0:00:00";
        self.gameHasStarted = NO;
    }
    self.cardsRemaining = nil;
}

- (IBAction)newGameButtonClicked:(id)sender {
    int itemsInSection, cardSelection;
    for (CardCollectionViewCell *cell in self.collectionView.visibleCells){
        [cell hideCard];
    }
    
    cardSelection = [[MenuModel sharedManager] cardSelectionSegment];
    if ([[MenuModel sharedManager] gridSizeSegment] == 0) {
        itemsInSection = 4;
    }
    else if ([[MenuModel sharedManager] gridSizeSegment] == 1){
        itemsInSection = 16;
    }
    else {
        itemsInSection = 36;
        
    }
    self.gameModel.gameBoardDeck = [NSMutableArray array];
    [self.collectionView reloadData];
    [[TimerModel sharedManager] cancelTimer];
    self.gameHasStarted = NO;
    self.cardsRemaining = nil;
    self.timerLabel.text = @"0:00:00";
}

#pragma mark - Timer Delegate
- (void) timerHasBeenUpdatedWithString:(NSString *)timerString{
    self.timerLabel.text = timerString;
}

#pragma mark - Alert Delegate / Methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
    }
    else if (buttonIndex == 1){
        [self newGameButtonClicked:self];
    }
    else
        NSLog(@"err");
}
- (void) showGameWonAlert {
UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"You Won the Game!"
                                                message:@"Press New Game to Play Again."
                                               delegate:self
                                      cancelButtonTitle:@"Cancel"
                                      otherButtonTitles:nil];
    [alert addButtonWithTitle:@"New Game!"];
    
    [alert show];
}

- (void) addTimeToArray:(NSString *) time{
    [[[TimerModel sharedManager] bestTimes] addObject:time];
    [[[TimerModel sharedManager] bestTimes] sortUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    if ([[[TimerModel sharedManager] bestTimes] count] > 5) {
        NSRange range = NSMakeRange(5, [[[TimerModel sharedManager] bestTimes] count] - 5);
        [[[TimerModel sharedManager] bestTimes] removeObjectsInRange:range];
    }
    NSLog(@"%@", [[[TimerModel sharedManager] bestTimes] description]);
    [[TimerModel sharedManager] saveCurrentAndBestTimes];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier] isEqualToString:@"menuSegue"] && [[MenuModel sharedManager] timeMode]) {
        [[TimerModel sharedManager] pauseTimer];
    }
    else if ([[segue identifier] isEqualToString:@"backSegue"] && [[MenuModel sharedManager] timeMode]){
        [[TimerModel sharedManager] resumeTimer];
    }
}
@end
