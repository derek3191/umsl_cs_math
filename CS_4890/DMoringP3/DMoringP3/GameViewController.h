//
//  ViewController.h
//  DMoringP3
//
//  Created by Derek Moring on 11/3/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIAlertViewDelegate>

@end
