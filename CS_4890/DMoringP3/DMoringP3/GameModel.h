//
//  GameModel.h
//  DMoringP3
//
//  Created by Derek Moring on 11/19/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameModel : NSObject

-(void) movePlistsToArrays;
-(void) createDeckOfCardsWithFace:(int) cardSelection andDeckSize:(NSInteger)halfGridSize;
-(void) shuffleDeck;

@property (nonatomic, strong) NSMutableArray *selectedCards;
@property (nonatomic, strong) NSMutableArray *gameBoardDeck;
@end
