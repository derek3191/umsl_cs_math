//
//  MenuViewController.m
//  DMoringP3
//
//  Created by Derek Moring on 11/5/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "MenuViewController.h"
#import "MenuModel.h"
#import "TimerModel.h"
#pragma mark - Private Declarations
@interface MenuViewController ()
- (IBAction)saveMenuChanges:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *gridSizeOutlet;
@property (weak, nonatomic) IBOutlet UISegmentedControl *cardChoiceOutlet;
@property (weak, nonatomic) IBOutlet UISwitch *timeSwitchOutlet;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *bestTimesOutletCollection;

@property TimerModel *timerModel;
@end
#pragma mark - Implementation
@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setMenuValues];
    self.timerModel = [TimerModel sharedManager];
}

- (void)viewWillAppear:(BOOL)animated {
    [self setMenuValues];
    if ([[[TimerModel sharedManager] bestTimes] count] > 0) {
        for (UILabel *it in _bestTimesOutletCollection) {
            if (it.tag < [self.timerModel.bestTimes count]) {
                [it setHidden:NO];
                it.text = [self.timerModel.bestTimes objectAtIndex:it.tag];
            }
            else{
                [it setHidden:YES];
            }
        }
    }
}

#pragma mark - Handle Menu Outlets/Values
- (void) setMenuValues {
    [[MenuModel sharedManager] getUserDefaults];
    // Display saved grid size
    if ([[MenuModel sharedManager] gridSizeSegment] == 0) {
        [self.gridSizeOutlet setSelectedSegmentIndex:0];
    }
    else if ([[MenuModel sharedManager] gridSizeSegment] == 1){
        [self.gridSizeOutlet setSelectedSegmentIndex:1];
    }
    else {
        [self.gridSizeOutlet setSelectedSegmentIndex:2];
    }
    
    // Display saved array
    if ([[MenuModel sharedManager] cardSelectionSegment] == 0) {
        [self.cardChoiceOutlet setSelectedSegmentIndex:0];
    }
    else if ([[MenuModel sharedManager] cardSelectionSegment] == 1){
        [self.cardChoiceOutlet setSelectedSegmentIndex:1];
    }
    else if ([[MenuModel sharedManager] cardSelectionSegment] == 2){
        [self.cardChoiceOutlet setSelectedSegmentIndex:2];
    }
    else{
        [self.cardChoiceOutlet setSelectedSegmentIndex:3];
    }
    
    // Display Time mode
    if ([[MenuModel sharedManager] timeMode] == YES) {
        [self.timeSwitchOutlet setOn:YES];
    }
    else
        [self.timeSwitchOutlet setOn:NO];
}

- (IBAction)saveMenuChanges:(id)sender {
    NSInteger grid, card;
    BOOL time;
    // Save the Grid Size
    if (self.gridSizeOutlet.selectedSegmentIndex == 0) {
        grid = 0;
    }
    else if (self.gridSizeOutlet.selectedSegmentIndex == 1){
        grid = 1;
    }
    else {
        grid = 2;
    }
    
    // Save the Card Selection
    if (self.cardChoiceOutlet.selectedSegmentIndex == 0) {
        card = 0;
    }
    else if (self.cardChoiceOutlet.selectedSegmentIndex == 1){
        card = 1;
    }
    else if (self.cardChoiceOutlet.selectedSegmentIndex == 2){
        card = 2;
    }
    else {
        card = 3;
    }
    
    // Save the Time Mode
    if (self.timeSwitchOutlet.isOn == YES) {
        time = YES;
    }
    else
        time = NO;
    
    
    [[MenuModel sharedManager] updateGrid:grid andCards:card andTime:time];

    
    [[MenuModel sharedManager] saveUserDefaults];
}




@end
