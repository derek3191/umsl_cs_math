//
//  MenuModel.m
//  DMoringP3
//
//  Created by Derek Moring on 11/5/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "MenuModel.h"
#pragma mark - Implementation
@implementation MenuModel


+ (id)sharedManager{
    static MenuModel *menuManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        menuManager = [[MenuModel alloc]init];
    });
    return menuManager;
}

// Method to get the default gameboard values from controller
- (void) updateGrid:(NSInteger)grid andCards:(NSInteger)card andTime:(BOOL)time {
    self.gridSizeSegment = grid;
    self.cardSelectionSegment = card;
    self.timeMode = time;
}
#pragma mark - Persistence
- (void) saveUserDefaults{
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    
    NSNumber *gridSizeObject = [NSNumber numberWithInteger:self.gridSizeSegment];
    NSNumber *cardSelectionObject = [NSNumber numberWithInteger:self.cardSelectionSegment];
    NSNumber *timeModeObject = [NSNumber numberWithBool:self.timeMode];
    
    [settings setObject:timeModeObject forKey:@"timeMode"];
    [settings setObject:gridSizeObject forKey:@"gridSize"];
    [settings setObject:cardSelectionObject forKey:@"cardSelection"];
    
    [settings synchronize];
}

-(void)getUserDefaults{
    NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
    
    NSNumber *gridSizeObject, *cardSelectionObject, *timeModeObject;
    
    timeModeObject = [settings objectForKey:@"timeMode"];
    gridSizeObject = [settings objectForKey:@"gridSize"];
    cardSelectionObject= [settings objectForKey:@"cardSelection"];
    
    self.gridSizeSegment = gridSizeObject.integerValue;
    self.cardSelectionSegment = cardSelectionObject.integerValue;
    self.timeMode = timeModeObject.boolValue;
}
@end
