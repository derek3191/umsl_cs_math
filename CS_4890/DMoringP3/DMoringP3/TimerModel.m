//
//  TimerModel.m
//  DMoringP3
//
//  Created by Derek Moring on 11/22/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "TimerModel.h"

#pragma mark - Private Declaration
@interface TimerModel()
@property (nonatomic, strong) NSTimer *timerTimer;

@property (nonatomic) int currentPrivateSecondTime;
@property (nonatomic) int currentPrivateMinuteTime;
@property (nonatomic) int currentPrivateHourTime;

@property (nonatomic, strong) NSString *formattedStopwatchStringValue;



@end

#pragma mark - Implementation
@implementation TimerModel
- (instancetype)init
{
    self = [super init];
    if (self) {
        _currentPrivateSecondTime = 0;
        _currentPrivateMinuteTime = 0;
        _currentPrivateHourTime = 0;
        _bestTimes = [NSMutableArray array];
        //[self getCurrentAndBestTimes]; //Part of how I tried to save the currentTime and Best Times
    }
    return self;
}

+ (id) sharedManager {
    static TimerModel *timerManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        timerManager = [[TimerModel alloc]init];
    });
    return timerManager;
}

#pragma mark - Timer Methods
- (void) startTimer{
    //_currentPrivateHourTime = [_startHours intValue];
    //_currentPrivateMinuteTime = [_startMinutes intValue];
    
    _isRunning = YES;
    _hasEnded = NO;
    _timerTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimer) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_timerTimer forMode:NSRunLoopCommonModes];
}

- (void) updateTimer {
    if (_currentPrivateSecondTime < 59) {
        _currentPrivateSecondTime++;
    }
    else {
        _currentPrivateSecondTime = 0;
        if (_currentPrivateMinuteTime < 59) {
            _currentPrivateMinuteTime++;
        }
        else {
            _currentPrivateMinuteTime = 0;
                _currentPrivateHourTime++;
        }
    }
    _formattedStopwatchStringValue = [NSString stringWithFormat:@"%2d:%02d:%02d", _currentPrivateHourTime, _currentPrivateMinuteTime, _currentPrivateSecondTime];
    
    [self.delegate timerHasBeenUpdatedWithString:_formattedStopwatchStringValue];
    
}

- (void)cancelTimer{
    [_timerTimer invalidate];
    _timerTimer = nil;
    _currentPrivateHourTime = [_startHours intValue];
    _currentPrivateMinuteTime = [_startMinutes intValue];
    _currentPrivateSecondTime = 0;
}
- (void)pauseTimer {
    [_timerTimer invalidate];
    _timerTimer = nil;
}
- (void)resumeTimer {
    [self startTimer];
}

- (void)saveCurrentAndBestTimes {
    NSError *error;
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES);
    NSString *directory = [pathArray objectAtIndex:0];
    NSString *saveTimePath = [directory stringByAppendingString:@"saveTime.plist"];
    NSString *bestTimePath = [directory stringByAppendingString:@"bestTime.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:saveTimePath]) {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"saveTime" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath:saveTimePath error:&error];
    }
    
    if (![fileManager fileExistsAtPath:bestTimePath]){
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"bestTime" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath:bestTimePath error:&error];
    }
    
    NSMutableDictionary *currentTimeDict = [[NSMutableDictionary alloc] initWithContentsOfFile:saveTimePath];
    
    [currentTimeDict setObject:[NSNumber numberWithInt:_currentPrivateHourTime] forKey:@"Hour"];
    [currentTimeDict setObject:[NSNumber numberWithInt:_currentPrivateMinuteTime] forKey:@"Minute"];
    [currentTimeDict setObject:[NSNumber numberWithInt:_currentPrivateSecondTime] forKey:@"Second"];
    
    [currentTimeDict writeToFile:saveTimePath atomically:YES];
    [_bestTimes writeToFile:bestTimePath atomically:YES];
}

- (void) getCurrentAndBestTimes {
    NSError *error;
    NSArray *pathArray = NSSearchPathForDirectoriesInDomains(NSDocumentationDirectory, NSUserDomainMask, YES);
    NSString *directory = [pathArray objectAtIndex:0];
    NSString *saveTimePath = [directory stringByAppendingString:@"saveTime.plist"];
    NSString *bestTimePath = [directory stringByAppendingString:@"bestTime.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath:saveTimePath]) {
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"saveTime" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath:saveTimePath error:&error];
    }
    
    if (![fileManager fileExistsAtPath:bestTimePath]){
        NSString *bundle = [[NSBundle mainBundle] pathForResource:@"bestTime" ofType:@"plist"];
        [fileManager copyItemAtPath:bundle toPath:bestTimePath error:&error];
    }
    
    NSMutableDictionary *currentTimeDict = [[NSMutableDictionary alloc] initWithContentsOfFile:saveTimePath];

    _currentPrivateHourTime = [[currentTimeDict valueForKey:@"Hour"] intValue];
    _currentPrivateMinuteTime = [[currentTimeDict valueForKey:@"Minute"]intValue];
    _currentPrivateSecondTime = [[currentTimeDict valueForKey:@"Second"]intValue];
    
    _bestTimes = [NSMutableArray arrayWithContentsOfFile:bestTimePath];

}
@end