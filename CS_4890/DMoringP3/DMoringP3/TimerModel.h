//
//  TimerModel.h
//  DMoringP3
//
//  Created by Derek Moring on 11/22/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol timerDelegate <NSObject>

-(void) timerHasBeenUpdatedWithString:(NSString *)timerString;

@end

#pragma mark - Public Declaration
@interface TimerModel : NSObject
@property (nonatomic) BOOL isRunning;
@property (nonatomic) BOOL hasEnded;
@property (nonatomic, strong) NSNumber *startHours;
@property (nonatomic, strong) NSNumber *startMinutes;
@property (nonatomic, strong) NSString *timeRemainingString;
@property (nonatomic, strong) NSMutableArray *bestTimes;
@property (nonatomic, weak) id<timerDelegate> delegate;
+(id)sharedManager;
-(void)startTimer;
-(void)cancelTimer;
-(void)pauseTimer;
-(void)resumeTimer;
- (void)saveCurrentAndBestTimes;
- (void)getCurrentAndBestTimes;
@end
