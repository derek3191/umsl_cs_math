//
//  GameModel.m
//  DMoringP3
//
//  Created by Derek Moring on 11/19/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "GameModel.h"
#pragma mark - Private Declaration
@interface GameModel()

@property (nonatomic, strong) NSArray *foodArray;
@property (nonatomic, strong) NSArray *peopleArray;
@property (nonatomic, strong) NSArray *animalArray;
@property (nonatomic, strong) NSArray *clockArray;

@end
#pragma mark - Implementation
@implementation GameModel

#pragma mark - Create Potential Decks
-(void) movePlistsToArrays {
    _foodArray = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Food" ofType:@"plist" ]];
    _peopleArray = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"People" ofType:@"plist" ]];
    _animalArray = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Animals" ofType:@"plist" ]];
    _clockArray = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Clock" ofType:@"plist" ]];
    _gameBoardDeck = [NSMutableArray array];
    _selectedCards = [NSMutableArray array];
    

}

#pragma mark - Prepare Deck
-(void) createDeckOfCardsWithFace:(int) cardSelection andDeckSize:(NSInteger)halfGridSize{
    NSMutableArray *sourceArray;
    if (cardSelection == 0) {
        sourceArray = [NSMutableArray arrayWithArray:_foodArray];
    }
    else if (cardSelection == 1){
        sourceArray = [NSMutableArray arrayWithArray:_peopleArray];
    }
    else if (cardSelection == 2){
        sourceArray = [NSMutableArray arrayWithArray:_animalArray];
    }
    else
        sourceArray = [NSMutableArray arrayWithArray:_clockArray];
    
    for (int i = 0; i < halfGridSize; i++) {
        int randomIndex = arc4random_uniform([sourceArray count]);
        [_gameBoardDeck addObject:[sourceArray objectAtIndex:randomIndex]];
        [sourceArray removeObjectAtIndex:randomIndex];
    }
    [_gameBoardDeck addObjectsFromArray:_gameBoardDeck];
    [self shuffleDeck];
}

-(void) shuffleDeck {
    int randomIndex1, arraySize;
    arraySize = [_gameBoardDeck count];
    for (int i = 0 ; i < arraySize; i++) {
        randomIndex1 = arc4random_uniform(arraySize);
        [_gameBoardDeck exchangeObjectAtIndex:randomIndex1 withObjectAtIndex:i];
    }
}

@end
