//
//  CardCollectionViewCell.m
//  DMoringP3
//
//  Created by Derek Moring on 11/12/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "CardCollectionViewCell.h"

@implementation CardCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void)showCard{
    [UIView transitionWithView:self.contentView
                      duration:0.3
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        [self.cardImage setImage:[UIImage imageNamed:@"face.png"]];
                        [self.cardLabel setHidden:NO];
                    } completion:nil];
}

- (void)hideCard{
    [UIView transitionWithView:self.contentView
                      duration:0.3
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:^{
                        [self.cardImage setImage:[UIImage imageNamed:@"back.png"]];
                        [self.cardLabel setHidden:YES];
                        
                    } completion:nil];
}

@end
