//
//  CompassView.h
//  Compass
//
//  Created by Derek Moring on 10/29/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompassLayer.h"

@interface CompassView : UIView
@property(nonatomic, strong) CompassLayer *rotationLayer;
@end
