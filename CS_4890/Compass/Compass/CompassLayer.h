//
//  CompassLayer.h
//  Compass
//
//  Created by Derek Moring on 10/29/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface CompassLayer : CALayer

@end
