//
//  CompassLayer.m
//  Compass
//
//  Created by Derek Moring on 10/29/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "CompassLayer.h"

@implementation CompassLayer
- (void) layoutSublayers {
    CAShapeLayer *circle = [CAShapeLayer new];
    circle.contentsScale = [[UIScreen mainScreen] scale];
    circle.lineWidth = 2.0;
    circle.fillColor = [[UIColor colorWithRed:0.9 green:0.95 blue:0.9 alpha:0.9] CGColor];
    circle.strokeColor = [[UIColor grayColor ]CGColor];
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddEllipseInRect(path, nil, CGRectInset(self.bounds, 3, 3));
    circle.path = path;
    
    [self addSublayer:circle];
    circle.bounds = self.bounds;
    circle.position = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
}
@end
