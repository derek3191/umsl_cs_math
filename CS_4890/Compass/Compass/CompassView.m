//
//  CompassView.m
//  Compass
//
//  Created by Derek Moring on 10/29/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "CompassView.h"

@implementation CompassView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}



// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    [self.rotationLayer setFrame:self.bounds]; //visibible portion of screen
    [self.layer addSublayer:self.rotationLayer];
    
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.rotationLayer = [CompassLayer layer];
    }
    return self;
}


@end
