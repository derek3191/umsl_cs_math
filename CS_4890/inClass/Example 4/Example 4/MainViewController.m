//
//  MainViewController.m
//  Example 4
//
//  Created by David Vaughn on 9/16/14.
//  Copyright (c) 2014 Vigilante Studios, LLC. All rights reserved.
//

#import "MainViewController.h"
#import "MainControllerModel.h"
#import "CarsListingViewController.h"

@interface MainViewController () <MainModelDelegate>
@property (weak, nonatomic) IBOutlet UIButton *fetchCarsButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingCarsActivityIndicator;
@property (nonatomic, strong) MainControllerModel *model;
@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.model = [[MainControllerModel alloc] initWithDelegate:self];
}

- (IBAction)fetchCarsButtonPressed:(id)sender
{
	[self.model getWebDataForCars];
	[self.loadingCarsActivityIndicator startAnimating];
	[self.loadingCarsActivityIndicator setHidden:NO];
	[self.fetchCarsButton setEnabled:NO];
	[self.view setAlpha:0.6f];
}

- (void)receivedDataForCars
{
	[self.loadingCarsActivityIndicator stopAnimating];
	[self.loadingCarsActivityIndicator setHidden:YES];
	[self.view setAlpha:1.0f];
	[self performSegueWithIdentifier:@"showCars" sender:self];
}

@end
