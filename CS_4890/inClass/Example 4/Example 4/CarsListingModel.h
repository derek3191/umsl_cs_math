//
//  CarsListingModel.h
//  Example 4
//
//  Created by David Vaughn on 9/17/14.
//  Copyright (c) 2014 Vigilante Studios, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CarsListingModel : NSObject
@property (nonatomic, strong) NSArray *allCarsArray;
@end
