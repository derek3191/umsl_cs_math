//
//  MainControllerModel.h
//  Example 4
//
//  Created by David Vaughn on 9/17/14.
//  Copyright (c) 2014 Vigilante Studios, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MainModelDelegate <NSObject>
- (void)receivedDataForCars;
@end

@interface MainControllerModel : NSObject
@property (nonatomic, weak) id<MainModelDelegate> delegate;
@property (nonatomic, readonly) NSArray *carsArray;
- (id)initWithDelegate:(id<MainModelDelegate>)delegate;
- (void)getWebDataForCars;
@end
