//
//  HTTPRequestHandler.h
//  Example 4
//
//  Created by David Vaughn on 9/17/14.
//  Copyright (c) 2014 Vigilante Studios, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HTTPRequestHandler : NSObject
- (void)startWebRequestWithCompletionBlock:(void(^)(NSArray *dataArray))completionBlock;
@end
