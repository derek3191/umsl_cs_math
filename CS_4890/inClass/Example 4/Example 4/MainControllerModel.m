//
//  MainControllerModel.m
//  Example 4
//
//  Created by David Vaughn on 9/17/14.
//  Copyright (c) 2014 Vigilante Studios, LLC. All rights reserved.
//

#import "MainControllerModel.h"
#import "HTTPRequestHandler.h"

@interface MainControllerModel()
@property (nonatomic, strong) NSArray *carsArray;
@end

@implementation MainControllerModel

- (id)initWithDelegate:(id<MainModelDelegate>)delegate
{
	self = [super init];
	if (self) {
		self.delegate = delegate;
	}
	return self;
}

- (void)getWebDataForCars
{
	HTTPRequestHandler *requestHandler = [[HTTPRequestHandler alloc] init];
	[requestHandler startWebRequestWithCompletionBlock:^(NSArray *dataArray) {
		self.carsArray = dataArray;
		[self.delegate receivedDataForCars];
	}];
}

@end
