//
//  main.m
//  Example 4
//
//  Created by David Vaughn on 9/16/14.
//  Copyright (c) 2014 Vigilante Studios, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
	}
}
