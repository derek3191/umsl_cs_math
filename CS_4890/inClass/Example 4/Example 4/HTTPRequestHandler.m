//
//  HTTPRequestHandler.m
//  Example 4
//
//  Created by David Vaughn on 9/17/14.
//  Copyright (c) 2014 Vigilante Studios, LLC. All rights reserved.
//

#import "HTTPRequestHandler.h"

@interface HTTPRequestHandler() <NSURLConnectionDelegate, NSURLConnectionDataDelegate>
@property (nonatomic, strong) NSURLConnection *mainConnection;
@property (nonatomic, strong) NSMutableData *returnedData;
@property (nonatomic, copy) void(^completionCallback)(NSArray *dataArray);
@end

@implementation HTTPRequestHandler

- (instancetype)init
{
	self = [super init];
	if (self) {
		
	}
	return self;
}

#pragma mark -
#pragma mark public methods
- (void)startWebRequestWithCompletionBlock:(void(^)(NSArray *))completionBlock
{
	self.completionCallback = completionBlock;
	NSURL *url = [[NSURL alloc] initWithString:@"http://www.workstation4.com/cars.json"];
	NSMutableURLRequest *mutableRequest = [[NSMutableURLRequest alloc] initWithURL:url];
	
	[mutableRequest setHTTPMethod:@"GET"];
	[mutableRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
	self.mainConnection = [[NSURLConnection alloc] initWithRequest:mutableRequest delegate:self startImmediately:NO];
	[self.mainConnection start];
}

#pragma mark -
#pragma mark NSURLConnection DataDelegate and Delegate methods
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
	self.completionCallback(nil);
	self.completionCallback = nil;
	NSLog(@"did fail with error: %@", error);
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
	NSLog(@"did receive response: %@", response);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	if (!self.returnedData) {
		self.returnedData = [[NSMutableData alloc] init];
	}
	[self.returnedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	NSError *error;
	id returnData = [NSJSONSerialization JSONObjectWithData:self.returnedData options:NSJSONReadingMutableContainers | NSJSONReadingMutableLeaves error:&error];
	
	if ([returnData isKindOfClass:[NSArray class]]) {
		self.completionCallback(returnData);
	} else {
		self.completionCallback(nil);
	}
		
	self.completionCallback = nil;
	self.mainConnection = nil;
	NSLog(@"connection did finish loading");
}

@end
