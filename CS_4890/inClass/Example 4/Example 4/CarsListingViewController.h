//
//  CarsListingViewController.h
//  Example 4
//
//  Created by David Vaughn on 9/17/14.
//  Copyright (c) 2014 Vigilante Studios, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CarsListingModel.h"

@interface CarsListingViewController : UIViewController
@property (nonatomic, strong) CarsListingModel *model;
@end
