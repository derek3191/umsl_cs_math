//
//  Example_4Tests.m
//  Example 4Tests
//
//  Created by David Vaughn on 9/16/14.
//  Copyright (c) 2014 Vigilante Studios, LLC. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Example_4Tests : XCTestCase

@end

@implementation Example_4Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
