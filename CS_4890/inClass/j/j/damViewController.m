//
//  damViewController.m
//  j
//
//  Created by Derek Moring on 9/24/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "damViewController.h"

@interface damViewController ()
@property int (^newBlock)(int num1, int num2);
@end

@implementation damViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(BOOL)canPerformUnwindSegueAction:(SEL)action fromViewController:(UIViewController *)fromViewController withSender:(id)sender
@end
