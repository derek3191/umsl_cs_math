//
//  main.m
//  j
//
//  Created by Derek Moring on 9/24/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "damAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([damAppDelegate class]));
    }
}
