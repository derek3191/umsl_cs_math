//
//  Cars.m
//  Add Core Data
//
//  Created by Derek Moring on 10/13/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "Cars.h"


@implementation Cars

@dynamic car_id;
@dynamic year;
@dynamic model;
@dynamic make;

@end
