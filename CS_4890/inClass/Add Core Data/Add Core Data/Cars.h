//
//  Cars.h
//  Add Core Data
//
//  Created by Derek Moring on 10/13/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Cars : NSManagedObject

@property (nonatomic, retain) NSNumber * car_id;
@property (nonatomic, retain) NSString * year;
@property (nonatomic, retain) NSString * model;
@property (nonatomic, retain) NSString * make;

@end
