//
//  damAppDelegate.h
//  Core Graphics
//
//  Created by Derek Moring on 10/1/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface damAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
