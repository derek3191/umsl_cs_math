//
//  MyGraphicsView.m
//  Core Graphics
//
//  Created by Derek Moring on 10/1/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import "MyGraphicsView.h"

@implementation MyGraphicsView

// 1st thing we need CGContext
/*
- (void)drawRect:(CGRect)rect
{
    //Create path
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextBeginPath(context);
    CGContextMoveToPoint(context, 100.0, 150.0); //over 100.0 and down 150.0 and make a point
    CGContextAddLineToPoint(context, 200.0, 250.0); // over 200 down 250 add a point
    CGContextAddLineToPoint(context, 200.0, 350.0); // over 200 down 350 add a point
    CGContextAddLineToPoint(context, 100.0, 350.0); // over 100 down 350 add a point
    CGContextClosePath(context); //conects last point to starting point
    CGContextStrokePath(context); //1px wide black is default
    
    // acts like array... saves settings
    CGContextSaveGState(context);
    
    //create another path
    CGContextBeginPath(context);
    CGContextAddArc(context, 100.0, 150.0, 100.0, 0, 2*M_PI, 0); //x and y are start or origin???
    CGContextSetFillColorWithColor(context, [[UIColor blueColor] CGColor]);
    CGContextFillPath(context);
    
    //add a stroke to the circle
    CGContextAddArc(context, 100.0, 150.0, 100.0, 0, 2*M_PI, 0); //x and y are start or origin???
    CGContextSetStrokeColorWithColor(context, [[UIColor redColor]CGColor]);
    CGContextStrokePath(context);

            // THIS WILL DRAW A RED CIRCLE
            // CGContextAddArc(context, 200.0, 300.0, 50.0, 0, 2*M_PI, 0);
            //CGContextStrokePath(context);
    
    // pops of settings saved on line 27
    CGContextRestoreGState(context);
    
    // THE CIRCLE IS BLACK NOW
    CGContextAddArc(context, 200.0, 300.0, 50.0, 0, 2*M_PI, 0);
    CGContextStrokePath(context);
}
*/

- (void)drawRect:(CGRect)rect
{
    //Create path
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(context);
//    CGContextAddArc(context, 200.0, 100.0, 80.0, 0, 2*M_PI, 0);

    CGContextBeginPath(context);
    CGContextMoveToPoint(context, 100.0, 150.0); //over 100.0 and down 150.0 and make a point
    CGContextAddLineToPoint(context, 200.0, 250.0); // over 200 down 250 add a point
    CGContextAddLineToPoint(context, 200.0, 350.0); // over 200 down 350 add a point
    CGContextAddLineToPoint(context, 100.0, 350.0); // over 100 down 350 add a point
    CGContextClosePath(context); //conects last point to starting point
//    CGContextStrokePath(context); //1px wide black is default
    CGContextClip(context);

    CGFloat locations[3] = { 0.0, 0.5, 1.0 };
    NSArray *colors = @[(id)[UIColor colorWithRed:0.0 green:0.0 blue:1.0 alpha:0.3].CGColor,
                        (id)[UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:1.0].CGColor,
                        (id)[UIColor colorWithRed:0.0 green:1.0 blue:0.0 alpha:0.3].CGColor];
    
    CGColorSpaceRef space = CGColorSpaceCreateDeviceRGB();
    
    CGGradientRef gradient = CGGradientCreateWithColors(space, (__bridge CFArrayRef)colors, locations);
    CGContextDrawLinearGradient(context, gradient, CGPointMake(100.0, 0.0), CGPointMake(200.0, 0.0), 0);
    CGColorSpaceRelease(space);
    CGGradientRelease(gradient);
    
    CGContextRestoreGState(context);
    
    CGContextAddArc(context, 200.0, 100.0, 80.0, 0, 2*M_PI, 0);
    CGContextSetFillColorWithColor(context, [[UIColor redColor]CGColor]);
    CGContextFillPath(context);
    
}


@end


