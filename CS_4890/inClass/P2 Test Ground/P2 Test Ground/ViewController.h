//
//  ViewController.h
//  P2 Test Ground
//
//  Created by Derek Moring on 10/9/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIDatePicker *dateOutlet;

- (IBAction)buttonClicked:(id)sender;
@end
