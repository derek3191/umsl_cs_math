//
//  MyOwnTableViewModel.h
//  sep3
//
//  Created by Derek Moring on 9/3/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyOwnTableViewModel : NSObject
@property(nonatomic, readonly) NSArray *tableRowsArray;

@end
