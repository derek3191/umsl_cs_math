//
//  main.m
//  sep3
//
//  Created by Derek Moring on 9/3/14.
//  Copyright (c) 2014 damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "damAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([damAppDelegate class]));
    }
}
