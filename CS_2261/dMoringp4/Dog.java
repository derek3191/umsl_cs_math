/**
 * 
 */
package proj4;

/**
 * @author dmoring
 * @project DMoringProj4	
 */
public class Dog extends Mammal{
	int hunger;
	int cleanliness;
	int fun;
	private int loyalty;
	
	public Dog(String name) {
		this.setName(name);
		fun = (int)(Math.random() * 100) + 1;
		hunger = (int)(Math.random() * 100) + 1;
		cleanliness = (int)(Math.random() * 100) + 1;
		loyalty = (int)(Math.random() * 100) + 1;
	}
	public int getHunger(){
		return hunger;
	}
	public void setHunger(int hunger){
		this.hunger = hunger;
	}
	public int getCleanliness(){
		return cleanliness;
	}
	public void setCleanliness(int cleanliness){
		this.cleanliness = cleanliness;
	}
	public int getFun(){
		return fun;
	}
	public void setFun(int fun){
		this.fun = fun;
	}
	public int getLoyalty(){
		return loyalty;
	}
	public void setLoyalty(int loyalty){
		this.loyalty = loyalty;
	}
	@Override
	public String toString(){
		return "" + this.getName() + "'s hunger:" + hunger + " Cleanliness:" + cleanliness + " Fun:" + fun + " Loyalty:" + loyalty;
	}
}
