/**
 * 
 */
package proj4;

import java.util.ArrayList;
import java.util.Scanner;
/**
 * @author dmoring
 * @project DMoringProj4	
 */
public class Human extends Mammal{
	int money;
	int dogFood;
	public ArrayList<Dog> dogList = new ArrayList<Dog> ();
	Scanner input = new Scanner(System.in);	
	String name;

	// Constructor takes in a string as a parameter
	// to create a name for the human. Then randomizes
	// the dogs in the arraylist and gets the names for them.
	public Human(String name){
		this.setName(name);
		int numDogs = (int)(Math.random() * 4 + 1);
		for (int i = 1; i <= numDogs; i++){
			System.out.println("What is the Dogs name?");
			String dogsName = input.nextLine();
			dogList.add(new Dog(dogsName));
		dogFood = (int)(Math.random() * 100 + 1);
		money = (int)(Math.random() * 10000 + 1);
		}
	}
	// The following are methods to do fun things with the humans
	// dogs.. It is being called by the main class but it doesn't 
	// seem to affect the arraylist of dogs. 
	public void walk(){
		for (int i = 0; i < dogList.size(); i++){
			dogList.get(i).setCleanliness(dogList.get(i).getCleanliness() - 5);
			dogList.get(i).setFun(dogList.get(i).getFun() + 5);
			dogList.get(i).setLoyalty(dogList.get(i).getLoyalty() + 1);
			dogList.get(i).setHunger(dogList.get(i).getHunger() + 5);
		}
	}
	public void bathe(){
		for (int i = 0; i < dogList.size(); i++){
			dogList.get(i).setCleanliness(100);
			dogList.get(i).setFun(dogList.get(i).getFun() - 5);
			dogList.get(i).setLoyalty(dogList.get(i).getLoyalty() - 1);
			dogList.get(i).setHunger(dogList.get(i).getHunger() - 5);
		}
	}
	public void feed(){
		for (int i = 0; i < dogList.size(); i++){
			dogList.get(i).setCleanliness(dogList.get(i).getCleanliness() - 5);
			dogList.get(i).setFun(dogList.get(i).getFun() + 5);
			dogList.get(i).setLoyalty(dogList.get(i).getLoyalty() + 1);
			dogList.get(i).setHunger(0);
		}
	}
	public void buyFood(){
		if (dogFood == 0){
			System.out.println("________________________");
			System.out.println("How much food would you like? (1-100):");
			int foodInput = input.nextInt();
			dogFood += foodInput;
			money -= foodInput;
			
		}
	}
	public void goWork(){
		for (int i = 0; i < dogList.size(); i++){
			dogList.get(i).setCleanliness(dogList.get(i).getCleanliness() - 5);
			dogList.get(i).setFun(dogList.get(i).getFun() + 5);
			dogList.get(i).setLoyalty(dogList.get(i).getLoyalty() + 1);
			dogList.get(i).setHunger(dogList.get(i).getHunger() + 5);
		}
		dogFood -= 5;
		money += 500;
	}
	public void passTheTime(){
		for (int i = 0; i < dogList.size(); i++){
			dogList.get(i).setCleanliness(dogList.get(i).getCleanliness() - 5);
			dogList.get(i).setFun(dogList.get(i).getFun() - 5);
			dogList.get(i).setLoyalty(dogList.get(i).getLoyalty());
			dogList.get(i).setHunger(dogList.get(i).getHunger() + 5);
		}
		dogFood -= 5;
		money += 100;
	}
	public void displayDogStats(){
		for (int i = 0; i < dogList.size(); i++){
			System.out.println(dogList.get(i).toString());
		}
	}
	@Override
	public String toString(){
		displayDogStats();
		return  "" + this.getName() + "'s stats: " + "Sex:" + this.displaySex() + " Weight:"+ this.getWeight() + " Dog Food:" +
	dogFood + " Money:$" + money + " Dogs:" + dogList.size();
	}
}
