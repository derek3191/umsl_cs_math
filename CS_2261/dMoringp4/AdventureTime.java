/**
 * 
 */
package proj4;
import java.util.ArrayList;
import java.util.Scanner;
/**
 * @author dmoring
 * @project DMoringProj4	
 */
public class AdventureTime {

	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		Scanner stringInput = new Scanner(System.in);
		
		System.out.println("How many humans should we have? ");
		int humans = input.nextInt();
		ArrayList<Human> humanList = new ArrayList<Human>();
		
		// This creates a list of all the humans..
		for (int i = 0; i < humans; i++){
			System.out.println("What is your humans name? ");
			String humanNames = stringInput.nextLine();
			humanList.add(new Human(humanNames));
		}
		
		// The first loop is infinite to display the menu. The
		// second loop asks what to do for each human. The next is
		// the actual menu.
		for (;;){
			for (int i = 0; i < humanList.size(); i++){
				System.out.println("What would you like to do with " + humanList.get(i).getName() + "?\n" 		
						+ "Pick a choice:\n" + "\tE: Walk the dog\n" + "\tB: Bathe the dog\n" + "\tF: Feed the dog\n" + 
						"\tP: Purchase dog food\n" + "\tW: Go to work\n" + "\tN: Do nothing\n" + "\tQ: Quit\n");
				String answer = stringInput.nextLine();
				for (int j=i ; j>i ; j++){
					if (answer.equals("e") || answer.equals("E")){
						humanList.get(i).walk();// call the walk dog function for the human	
					}
					else if (answer.equals("b") || answer.equals("B")){
						humanList.get(i).bathe();// call the bathe function for the human
					}
					else if (answer.equals("f") || answer.equals("F")){
						humanList.get(i).feed();// call the feed function for the human
					}	
					else if (answer.equals("p") || answer.equals("P")){
						humanList.get(i).buyFood();// call the buyFood function for human
					}
					else if (answer.equals("w") || answer.equals("W")){
						humanList.get(i).goWork();// call the go to work function for human
					}
					else if (answer.equals("n") || answer.equals("N")){
						humanList.get(i).passTheTime();// call the go to work function for human
					}
					else if (answer.equals("q") || answer.equals("Q")) {
						System.out.println("Program Stopped.");
						System.exit(0);
						
					}
				}
				humanList.get(i).passTheTime();
				System.out.println(humanList.get(i));
			}
		}
	}
}
