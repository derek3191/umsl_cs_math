/**
 * 
 */
package proj4;

import java.util.Random;

/**
 * @author dmoring
 * @project DMoringProj4	
 */
public class Mammal {
	private String name;
	private int weight;
	private boolean sex; //BOOL TRUE = MALE --- BOOL FALSE = FEMALE
	Random getSexBool = new Random();
	
	public Mammal(){
		//The mammal should weigh 50 pounds or more
		weight = (int)(Math.random() * 300) + 51; 
		sex = getSexBool.nextBoolean();
	}

	public String getName(){

		return name;
	}
	public void setName(String name){
		if (name == null){
			name = "John Doe";
		}
		this.name = name;
	}
	public double getWeight(){
		return weight;
	}
	public void setWeight(int weight){
		this.weight = weight;
	}
	public boolean isSex(){
		return sex;
	}
	public void setSex(boolean sex){
		this.sex = sex;
	}
	
	public String displaySex(){
		if (sex == true)
			return "male";
		else
			return "felmale";
	}

}

