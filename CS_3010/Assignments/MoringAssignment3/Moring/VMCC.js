/** DEREK MORING ASSIGNMENT 3 **/
/** JAVASCRIPT FOR VMCC.HTML **/
/*****************************/
			
function checkName(){
	var name = document.getElementById("Fname");

	var pos = name.value.search(/^[a-zA-Z]{2}[a-zA-Z]*$/);
	if (pos !=0) {
		alert("Please enter a name more than 2 characters long.");
		document.form.Fname.focus();
		return false;
		}
		else
			return true;
}


function checkPhone(){
	var phone = document.getElementById("phone");
	
	// Validate the format of the phone number.
	var pos = phone.value.search(/^\d{3}-\d{3}-\d{4}$/);
	if (pos !=0) {
		alert("Your number is not in the right form\n"
			 + "please use the form (xxx-xxx-xxxx).")
		document.form.phone.focus();
		return false;
	}
	else
		return true;
}

function checkZip(){
	var zip = document.getElementById("zip");
	var pos = zip.value.search(/^\d{5}$/);
	if (pos !=0) {
		alert("Your zipcode must be in the following\n"
			+ "format: (xxxxx).")
		document.form.zip.focus();
		return false;
	}
	else
		return true;
}

function validate(){
	
	// VALIDATES THE ZIP CODE RANGE
	var zip = document.getElementById("zip");
	var zPos1 = zip.value.search(/^[6][3-9][1-9][2-9][1-9]$/);
	var zPos2 = zip.value.search(/^[6][4-9][0-9][0-9][0-9]$/);
	// VALIDATES THE PHONE NUMBER"
	var phone = document.getElementById("phone");
	var pPos1 = phone.value.search(/[6][3][6].{9}$/); 
	var pPos2 = phone.value.search(/[3][1][4].{9}$/);
	var pPos3 = phone.value.search(/[5][1][2].{9}$/);
	
	// IF ZIP IS INELIGIBLE, IGNORE PHONE
	if (zPos1 !=0 && zPos2 !=0){
			alert("Service is not available in your area.");
			}
	else{
		// ZIP IS ELIGIBLE. PHONE INELIGIBLE
		if (pPos1 !=0 && pPos2 !=0 && pPos3 !=0){
			alert("Service IS available in your area!\n" +
			 		"However, your phone number is ineliglible.");
		}
		else{
			// ZIP IS ELIGIBLE. PHONE ELIGIBLE
			alert("Service IS available in your area\n"
				+ "and you can keep your current number!");
			}
		}
}
