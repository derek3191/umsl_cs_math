//
//  damViewController.h
//  buttonApp
//
//  Created by Derek Moring on 6/23/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface damViewController : UIViewController
- (IBAction)btnGreen:(id)sender;
- (IBAction)btnRed:(id)sender;
- (IBAction)btnYellow:(id)sender;




@end
