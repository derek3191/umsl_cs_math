//
//  damViewController.m
//  buttonApp
//
//  Created by Derek Moring on 6/23/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "damViewController.h"

@interface damViewController ()

@end

@implementation damViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnGreen:(id)sender {
    NSLog(@"Chose Green");
}

- (IBAction)btnRed:(id)sender {
    NSLog(@"Chose Red");
}

- (IBAction)btnYellow:(id)sender {
    NSLog(@"Chose Yellow");
}
@end
