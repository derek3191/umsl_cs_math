//
//  damViewController.h
//  delegate
//
//  Created by Derek Moring on 6/23/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface damViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *myPass;

@end
