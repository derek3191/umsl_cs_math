//
//  damViewController.m
//  outlet
//
//  Created by Derek Moring on 6/23/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "damViewController.h"

@interface damViewController ()

@end

@implementation damViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    i = 0;
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btn:(id)sender {
    NSArray *messages = @[@"First Message",
                          @"Second Message",
                          @"Third message", @"Go to Hell"];
    self.labelOutlet.text = messages[i];
    i++;
    i = i % [messages count];
    NSLog(@"size %i\n", [messages count]);
}
@end
