//
//  DAMViewController.h
//  collectionApp
//
//  Created by Derek Moring on 7/2/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAMViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutletCollection(UISwitch) NSArray *ccc;
- (IBAction)buttonPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *label;
- (IBAction)textButton:(id)sender;

@end
