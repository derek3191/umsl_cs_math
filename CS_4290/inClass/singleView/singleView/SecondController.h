//
//  SecondController.h
//  singleView
//
//  Created by Derek Moring on 7/16/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Counter.h"

@interface SecondController : UIViewController

@property Counter *cnt;
- (IBAction)incButtonTwo:(id)sender;

@end
