//
//  Counter.m
//  singleView
//
//  Created by Derek Moring on 7/16/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "Counter.h"

@implementation Counter

-(void)inc{
    _count++;
}

-(id) init {
    self = [super init];
    if (self) {
        _count = 0;
    }
    return self;
}
@end
