//
//  DAMViewController.h
//  singleView
//
//  Created by Derek Moring on 7/16/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAMViewController : UIViewController
- (IBAction)incButtonOne:(id)sender;

@end
