//
//  DAMViewController.m
//  singleView
//
//  Created by Derek Moring on 7/16/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "DAMViewController.h"
#import "Counter.h"
#import "SecondController.h"

@interface DAMViewController ()

@property Counter *cnt;
@end

@implementation DAMViewController

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"movingToSecond"]) {
        SecondController *to = segue.destinationViewController;
        to.cnt = self.cnt;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	self.cnt = [Counter new];
    NSLog(@"FIRST LOADED: %i", self.cnt.count);
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)incButtonOne:(id)sender {
     [self.cnt inc];
    
}
@end
