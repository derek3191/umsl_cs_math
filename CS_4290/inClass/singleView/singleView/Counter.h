//
//  Counter.h
//  singleView
//
//  Created by Derek Moring on 7/16/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Counter : NSObject

@property (readonly) int count;
-(void)inc;
@end
