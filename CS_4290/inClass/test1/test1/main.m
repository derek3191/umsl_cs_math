//
//  main.m
//  test1
//
//  Created by Derek Moring on 6/18/14.
//  Copyright (c) 2014 Derek Moring. All rights reserved.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // insert code here...
        int i;
        printf("enter integer: ");
        scanf("%i", &i);
        printf("data was %i\n",i);
    }
    return 0;
}

