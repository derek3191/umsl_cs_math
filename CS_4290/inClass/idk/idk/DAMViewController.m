//
//  DAMViewController.m
//  idk
//
//  Created by Derek Moring on 7/9/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "DAMViewController.h"

@interface DAMViewController ()
//to make this private it has to be in .h
@property NSArray *files;

@end

@implementation DAMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.files = @[@"",@"dice1.png",@"dice2.png",@"dice3.png",@"dice4.png",@"dice5.png",@"dice6.png"];
    //self.images = @[];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)roll:(id)sender {
    
}

- (IBAction)idka:(id)sender {
    int x = arc4random() %6 + 1;
    UIImage *img = [UIImage imageNamed:self.files[x]];
    [self.buttonOutlet setBackgroundImage:img forState:UIControlStateNormal];
}
@end
