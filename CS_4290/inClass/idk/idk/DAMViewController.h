//
//  DAMViewController.h
//  idk
//
//  Created by Derek Moring on 7/9/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAMViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *buttonOutlet;
- (IBAction)roll:(id)sender;
- (IBAction)idka:(id)sender;

@end
