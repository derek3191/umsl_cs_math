//
//  DAMViewController.m
//  fraction
//
//  Created by Derek Moring on 7/2/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "DAMViewController.h"

@interface DAMViewController ()

@end

@implementation DAMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonPressed:(id)sender {
    self.labelOutlet.text = self.textOutlet.text;
    [self.textOutlet resignFirstResponder];
}

-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    self.labelOutlet.text = @"Never go full retard";
    [self.textOutlet resignFirstResponder];
}

//+++++++++++++++++++++++++++++++++++++++++++++
//+   textFieldShouldBeginEditing is called   +
//+    when the user text field is in focus   +
//+++++++++++++++++++++++++++++++++++++++++++++
- (BOOL) textFieldShouldBeginEditing:(UITextField *)textField {
    self.labelOutlet.text = nil;
    return YES;
}
@end
