//
//  damViewController.m
//  newAppEx24
//
//  Created by Derek Moring on 6/25/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "damViewController.h"
#import "damCouterClass.h"

@interface damViewController ()

@end

@implementation damViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // 
    //self.countPtr = [[damCouterClass alloc] init];
    //
    self.countPtr = [damCouterClass new];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonPressed:(id)sender {
    [self.countPtr inc];
    [self.countPtr print];
    
}
@end
