//
//  damCouterClass.m
//  newAppEx24
//
//  Created by Derek Moring on 6/25/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "damCouterClass.h"

@implementation damCouterClass

-(void) print {
    NSLog(@"Current Counter Value = %i", self.c);
}
-(damCouterClass *) init {
    self = [super init]; // call the init of damCouterClass??
    if (self){      // if pointer is really pointing somewhere
        self.c = 15;
    }
    return self;
}
-(void) inc {
    self.c++;
}


@end
