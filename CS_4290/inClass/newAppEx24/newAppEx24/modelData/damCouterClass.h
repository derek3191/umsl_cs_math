//
//  damCouterClass.h
//  newAppEx24
//
//  Created by Derek Moring on 6/25/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface damCouterClass : NSObject

@property int c;
-(void) print;
-(damCouterClass *) init;
-(void) inc;


@end
