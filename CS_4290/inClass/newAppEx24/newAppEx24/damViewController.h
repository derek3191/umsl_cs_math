//
//  damViewController.h
//  newAppEx24
//
//  Created by Derek Moring on 6/25/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "damCouterClass.h"

@interface damViewController : UIViewController
- (IBAction)buttonPressed:(id)sender;
@property damCouterClass * countPtr;

@end
