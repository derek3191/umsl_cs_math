//
//  DAMViewController.h
//  try1
//
//  Created by Derek Moring on 6/30/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAMViewController : UIViewController
@property (weak, nonatomic) IBOutlet UISlider *sliderOutlet;
@property (weak, nonatomic) IBOutlet UIStepper *stepperOutlet;
- (IBAction)sliderMoved:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *switchOutlet;
- (IBAction)buttonPressed:(id)sender;

@end
