//
//  main.m
//  try1
//
//  Created by Derek Moring on 6/30/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DAMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DAMAppDelegate class]));
    }
}
