//
//  DAMSecondViewController.m
//  tabbedView
//
//  Created by Derek Moring on 7/16/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "DAMSecondViewController.h"

@interface DAMSecondViewController ()

@end

@implementation DAMSecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"SECOND LOADED");
}

-(void) viewDidAppear:(BOOL)animated {
    
    NSLog(@"SECOND APPEARED");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
