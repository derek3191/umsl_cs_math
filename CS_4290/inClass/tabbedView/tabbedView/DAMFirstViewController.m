//
//  DAMFirstViewController.m
//  tabbedView
//
//  Created by Derek Moring on 7/16/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "DAMFirstViewController.h"

@interface DAMFirstViewController ()

@end

@implementation DAMFirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"FIRST LOADED");
}

-(void) viewDidAppear:(BOOL)animated {
    NSLog(@"FIRST APPEARED");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
