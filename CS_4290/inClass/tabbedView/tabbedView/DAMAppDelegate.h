//
//  DAMAppDelegate.h
//  tabbedView
//
//  Created by Derek Moring on 7/16/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
