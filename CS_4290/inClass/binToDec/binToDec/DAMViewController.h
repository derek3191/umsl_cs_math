//
//  DAMViewController.h
//  binToDec
//
//  Created by Derek Moring on 6/30/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAMViewController : UIViewController
@property (strong, nonatomic) IBOutletCollection(UISwitch) NSArray *binSwitch;
@property (strong, nonatomic) IBOutletCollection(UITextField) NSArray *binField;

@end
