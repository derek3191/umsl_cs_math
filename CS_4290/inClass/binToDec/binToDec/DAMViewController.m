//
//  DAMViewController.m
//  binToDec
//
//  Created by Derek Moring on 6/30/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "DAMViewController.h"

@interface DAMViewController ()

@end

@implementation DAMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    while (YES) {
        [self switchIdk];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) switchIdk {
    for (UISwitch *switches in self.binSwitch) {
        for (UITextField *fields in self.binField) {
            if (switches.isOn){
             fields.text = @"1";
            }
            else
                fields.text = @"0";
        }
    }
}

@end
