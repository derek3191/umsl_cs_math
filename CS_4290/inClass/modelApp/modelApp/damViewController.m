//
//  damViewController.m
//  modelApp
//
//  Created by Derek Moring on 6/25/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "damViewController.h"

@interface damViewController ()

@end

@implementation damViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    // STARTS THE COUNTER AT 10
    self.counter = 10;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)counterButton:(id)sender {
    // self   setter       getter   + 1
    //[self setCounter:(self.counter + 1)];
    self.counter++;
    NSLog(@"counter Value = %i", self.counter);
    
    
}
@end
