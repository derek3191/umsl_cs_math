//
//  damViewController.h
//  modelApp
//
//  Created by Derek Moring on 6/25/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface damViewController : UIViewController

@property int counter;
- (IBAction)counterButton:(id)sender;

@end
