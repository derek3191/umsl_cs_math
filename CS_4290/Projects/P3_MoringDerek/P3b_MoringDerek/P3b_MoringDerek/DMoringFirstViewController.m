//
//  DMoringFirstViewController.m
//  P3b_MoringDerek
//
//  Created by Derek Moring on 7/23/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "DMoringFirstViewController.h"

@interface DMoringFirstViewController ()

@end

@implementation DMoringFirstViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Initialize cards and add them all to the singleton book
	DMoringAddressCard *card1 = [[DMoringAddressCard alloc] initWithName:@"First Guy" andAge:1 andEmail:@"firstGuy@nowhere" ];
    DMoringAddressCard *card2 = [[DMoringAddressCard alloc] initWithName:@"Second Guy" andAge:2 andEmail:@"secondGuy@nowhere" ];
    DMoringAddressCard *card3 = [[DMoringAddressCard alloc] initWithName:@"Third Guy" andAge:3 andEmail:@"thirdGut@nowhere" ];
    DMoringAddressCard *card4 = [[DMoringAddressCard alloc] initWithName:@"Fourth Guy" andAge:4 andEmail:@"fourthGut@nowhere" ];
    [[DMoringAddressBook sharedBook]addCard:card1];
    [[DMoringAddressBook sharedBook]addCard:card2];
    [[DMoringAddressBook sharedBook]addCard:card3];
    [[DMoringAddressBook sharedBook]addCard:card4];
    
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)nextPressed:(id)sender {
    
    // Create display to store the card being returned with
    // the nextCard method
    DMoringAddressCard *display = [DMoringAddressCard new];
    display = [[DMoringAddressBook sharedBook]nextCard];
    self.resultLabel.text = [display description:display];
}
@end
