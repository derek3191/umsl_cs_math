//
//  DMoringAddressBook.m
//  P3b_MoringDerek
//
//  Created by Derek Moring on 7/26/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "DMoringAddressBook.h"

@implementation DMoringAddressBook

static DMoringAddressBook *a = nil;

-(id) initWithName: (NSString *) name
{
    self = [super init];
    
    if (self) {
        self.bookName = [NSString stringWithString: name];
        self.book = [NSMutableArray array];
    }
    
    return self;
}

-(id) init
{
    return [self initWithName: @"NoName"];
}

-(void) addCard: (DMoringAddressCard *) theCard
{   if ([self.book indexOfObject:theCard] == NSNotFound)
    [self.book addObject: theCard];
else
    NSLog(@"Already in the book");
}

-(void) removeCard: (DMoringAddressCard *) theCard {
    [self.book removeObjectIdenticalTo: theCard];
}

-(NSUInteger) entries
{
    return [self.book count];
}

-(void) list
{
    NSLog (@"======== Contents of: %@ =========", self.bookName);
    for (DMoringAddressCard *theCard in self.book )
        [theCard print];
    NSLog (@"==================================================");
}

-(DMoringAddressCard *) lookup: (NSString *) nameToLook {
    for (DMoringAddressCard *card in self.book)
        if ([card.name caseInsensitiveCompare: nameToLook] == NSOrderedSame){
            //[card description:card];
            return card;
}
    return nil;
}
-(DMoringAddressCard *) emailLookup: (NSString *) email withName:(NSString *)n {
    for (DMoringAddressCard *card in self.book) {
        if ([card.email caseInsensitiveCompare:email] == NSOrderedSame) {
            return card;
        }
        else if ([card.name hasPrefix:n] || [card.name hasSuffix:n]) {
            if ([card.email caseInsensitiveCompare:email] == NSOrderedSame) {
                return card;
            }
        }
        else
            return nil;
        
    }
    return nil;
}

-(DMoringAddressCard *) nextCard {
    static int c = 0;
    NSMutableArray *arr = [[DMoringAddressBook sharedBook]book];
    if (c < [[DMoringAddressBook sharedBook]entries]){
        c++;
        return arr[c-1];
    }
    else {
        c = 0;
        return nil;
    }
}

+(DMoringAddressBook *) sharedBook{
    if (a == nil) {
        a = [[super alloc]init];
    }
    return a;
}

@end
