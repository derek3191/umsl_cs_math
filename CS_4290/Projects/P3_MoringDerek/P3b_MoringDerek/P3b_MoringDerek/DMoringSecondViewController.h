//
//  DMoringSecondViewController.h
//  P3b_MoringDerek
//
//  Created by Derek Moring on 7/23/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMoringAddressBook.h"

@interface DMoringSecondViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *searchTextfield;
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;


@end
