//
//  DMoringAddressCard.m
//  P3b_MoringDerek
//
//  Created by Derek Moring on 7/26/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "DMoringAddressCard.h"

@implementation DMoringAddressCard

-(void) print {
    NSLog(@"----Address Card -------");
    NSLog(@"Name: %@",self.name);
    NSLog(@"Age: %i", self.age);
    NSLog(@"Email: %@", self.email);
    NSLog(@"------------------------");
}
-(id) initWithName: (NSString*) n andAge: (int) a andEmail:(NSString*) e {
    self = [super init];
    if (self) {
        self.name = n;
        self.age = a;
        self.email = e;
    }
    return self;
}

-(NSString *) description: (DMoringAddressCard *)c {
    NSString *output = [NSString stringWithFormat:@"Name: %@ \nAge: %i\nEmail: %@",c.name, c.age,c.email];
    return output;
}

@end
