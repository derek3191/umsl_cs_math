//
//  DMoringSecondViewController.m
//  P3b_MoringDerek
//
//  Created by Derek Moring on 7/23/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "DMoringSecondViewController.h"

@interface DMoringSecondViewController ()


@end

@implementation DMoringSecondViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    if (textField == self.searchTextfield) {
        
        // Create dCard to store the card being looked up.
        // display the card if it matches the search.
        DMoringAddressCard *dCard = [DMoringAddressCard new];
        NSString *name;
        name = self.searchTextfield.text;
        dCard = [[DMoringAddressBook sharedBook]lookup:name];
        if (dCard == nil) {
            self.resultLabel.text = @"No contacts found...";
        }
        else{
            self.resultLabel.text = [dCard description:dCard];
        }
        [textField resignFirstResponder];
        
    }
    return YES;
}


@end
