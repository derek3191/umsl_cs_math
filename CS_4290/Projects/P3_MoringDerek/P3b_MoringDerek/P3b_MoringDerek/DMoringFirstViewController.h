//
//  DMoringFirstViewController.h
//  P3b_MoringDerek
//
//  Created by Derek Moring on 7/23/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMoringAddressBook.h"

@interface DMoringFirstViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

- (IBAction)nextPressed:(id)sender;
@end
