//
//  DMoringAppDelegate.h
//  P3b_MoringDerek
//
//  Created by Derek Moring on 7/23/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMoringAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
