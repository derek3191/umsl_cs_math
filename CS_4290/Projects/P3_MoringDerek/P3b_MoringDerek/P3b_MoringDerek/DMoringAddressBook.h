//
//  DMoringAddressBook.h
//  P3b_MoringDerek
//
//  Created by Derek Moring on 7/26/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DMoringAddressCard.h"

@interface DMoringAddressBook : NSObject


@property (nonatomic, copy) NSString *bookName;
@property (nonatomic, strong) NSMutableArray *book;

-(instancetype)   initWithName: (NSString *) name;
-(void) addCard: (DMoringAddressCard *) theCard;
-(void) removeCard: (DMoringAddressCard *) theCard;
-(NSUInteger)  entries;
-(void) list;
-(DMoringAddressCard *) lookup: (NSString *) nameToLook;
-(DMoringAddressCard *) emailLookup: (NSString *) email withName: (NSString *) n;
-(DMoringAddressCard *) nextCard;
+(DMoringAddressBook *) sharedBook;
@end
