//
//  DMoringAddressCard.h
//  P3b_MoringDerek
//
//  Created by Derek Moring on 7/26/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMoringAddressCard : NSObject


@property (copy, nonatomic) NSString *name, *email;
@property int age;

-(void) print;
-(id) initWithName: (NSString*) n andAge: (int) a andEmail:(NSString*) e;
-(NSString *) description: (DMoringAddressCard *)c;

@end
