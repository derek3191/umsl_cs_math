//
//  DMoringSecondViewController.h
//  P3a_MoringDerek
//
//  Created by Derek Moring on 7/26/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMoringFraction.h"

@interface DMoringSecondViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *num1Label;
@property (weak, nonatomic) IBOutlet UILabel *num2Label;
@property (weak, nonatomic) IBOutlet UILabel *denum1Label;
@property (weak, nonatomic) IBOutlet UILabel *denum2Label;

- (IBAction)addPressed:(id)sender;
- (IBAction)multPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *label;

@end
