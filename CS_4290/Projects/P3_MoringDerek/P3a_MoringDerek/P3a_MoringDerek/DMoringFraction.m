//
//  DMoringFraction.m
//  P3a_MoringDerek
//
//  Created by Derek Moring on 7/26/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "DMoringFraction.h"

@implementation DMoringFraction

static DMoringFraction *f = nil;


-(id) init {
    self = [super init];
    if (self) {
        _num1 = 0;
        _denum1 = 1;
        _num2 = 0;
        _denum2 = 1;
        _outN = 0;
        _outD = 0;
    }
    return self;
}

-(id) initWithNum1: (int) n1 andDenum1: (int)d1 andNum2: (int) n2 andDenum2: (int)d2{
    self = [super init];
    if (self) {
        _num1 = n1;
        _denum1 = d1;
        _num2 = n2;
        _denum2 = d2;
        _outN = 0;
        _outD = 0;
    }
    return self;
}

+(DMoringFraction*) sharedFraction{
    
    if (f == nil) {
        f = [[super alloc]init];
    }
    return f;
}
-(NSString*) description {
    return [[NSString alloc] initWithFormat: @"Fraction 1: %i/%i Fraction 2: %i/%i", f.num1, f.denum1, f.num2, f.denum2];
}

@end
