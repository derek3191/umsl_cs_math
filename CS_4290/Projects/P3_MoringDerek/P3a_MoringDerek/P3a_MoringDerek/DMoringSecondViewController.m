//
//  DMoringSecondViewController.m
//  P3a_MoringDerek
//
//  Created by Derek Moring on 7/26/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "DMoringSecondViewController.h"

@interface DMoringSecondViewController ()

@end

@implementation DMoringSecondViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *n1 = [NSString stringWithFormat:@"%i",[[DMoringFraction sharedFraction]num1]];
    NSString *n2 = [NSString stringWithFormat:@"%i",[[DMoringFraction sharedFraction]num2]];
    NSString *d1 = [NSString stringWithFormat:@"%i",[[DMoringFraction sharedFraction]denum1]];
    NSString *d2 = [NSString stringWithFormat:@"%i",[[DMoringFraction sharedFraction]denum2]];
    
    self.num1Label.text = n1;
    self.num2Label.text = n2;
    self.denum1Label.text = d1;
    self.denum2Label.text = d2;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)addPressed:(id)sender {
    int n1 = [[DMoringFraction sharedFraction]num1];
    int n2 = [[DMoringFraction sharedFraction]num2];
    int d1 = [[DMoringFraction sharedFraction]denum1];
    int d2 = [[DMoringFraction sharedFraction]denum2];
    
    if (d1 != 0 || d2 !=0) {
        if (d1 == d2) {
            [[DMoringFraction sharedFraction]setOutN:n1+n2];
            [[DMoringFraction sharedFraction]setOutD:d1];
        }
        else{
            [[DMoringFraction sharedFraction]setOutN:(n1*d2+n2*d1)];
            [[DMoringFraction sharedFraction]setOutD:(d1*d2)];
        }
    }
}

- (IBAction)multPressed:(id)sender {
    int dd, nn;
    dd = [[DMoringFraction sharedFraction]denum1] * [[DMoringFraction sharedFraction]denum2];
    nn = [[DMoringFraction sharedFraction]num1] * [[DMoringFraction sharedFraction]num2];
    [[DMoringFraction sharedFraction]setOutD:dd];
    [[DMoringFraction sharedFraction]setOutN:nn];
}
@end
