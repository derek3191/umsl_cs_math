//
//  DMoringViewController.h
//  P3a_MoringDerek
//
//  Created by Derek Moring on 7/24/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMoringFraction.h"

@interface DMoringViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *num1TextField;
@property (weak, nonatomic) IBOutlet UITextField *denum1TextField;
@property (weak, nonatomic) IBOutlet UITextField *num2TextField;
@property (weak, nonatomic) IBOutlet UITextField *denum2TextField;

@property (weak, nonatomic) IBOutlet UILabel *resultLabel;
- (IBAction)choosePressed:(id)sender;


@end
