//
//  DMoringFraction.h
//  P3a_MoringDerek
//
//  Created by Derek Moring on 7/26/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DMoringFraction : NSObject

@property int num1;
@property int denum1;
@property int num2;
@property int denum2;
@property int outN;
@property int outD;

-(id) init;
-(id) initWithNum1: (int) n1 andDenum1: (int)d1 andNum2: (int) n2 andDenum2: (int)d2;
+(DMoringFraction*) sharedFraction;
-(NSString*) description;
@end
