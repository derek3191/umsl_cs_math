//
//  Fraction.m
//  exc5
//
//  Created by Cezary Janikow on 3/20/14.
//  Copyright (c) 2014 Cezary Janikow. All rights reserved.
//



#import "Fraction.h"

@implementation Fraction

static Fraction *n1 = nil;
static Fraction *d1 = nil;
static Fraction *n2 = nil;
static Fraction *d2 = nil;

-(id) init {
    self = [super init];
    if (self) {
        _num1 = 0;
        _denum1 = 1;
        _num2 = 0;
        _denum2 = 1;
    }
    return self;
}

-(id) initWithNum1: (int) n1 andDenum1: (int)d1 andNum2: (int) n2 andDenum2: (int)d2{
    self = [super init];
    if (self) {
        _num1 = n1;
        _denum1 = d1;
        _num2 = n2;
        _denum2 = d2;
    }
    return self;
}

+(Fraction*) sharedFraction{
    if (n1 == nil) {
        n1 = [[super alloc]init];
    }
        return n1;
}

+(Fraction*) addFractions{
    return n1;
}

+(Fraction*) multFractions{
    return n1;
}

-(NSString*) description {
    return [[NSString alloc] initWithFormat: @"Fraction 1: %i/%i Fraction 2: %i/%i", self.num1, self.denum1, self.num2, self.denum2];
}

@end
