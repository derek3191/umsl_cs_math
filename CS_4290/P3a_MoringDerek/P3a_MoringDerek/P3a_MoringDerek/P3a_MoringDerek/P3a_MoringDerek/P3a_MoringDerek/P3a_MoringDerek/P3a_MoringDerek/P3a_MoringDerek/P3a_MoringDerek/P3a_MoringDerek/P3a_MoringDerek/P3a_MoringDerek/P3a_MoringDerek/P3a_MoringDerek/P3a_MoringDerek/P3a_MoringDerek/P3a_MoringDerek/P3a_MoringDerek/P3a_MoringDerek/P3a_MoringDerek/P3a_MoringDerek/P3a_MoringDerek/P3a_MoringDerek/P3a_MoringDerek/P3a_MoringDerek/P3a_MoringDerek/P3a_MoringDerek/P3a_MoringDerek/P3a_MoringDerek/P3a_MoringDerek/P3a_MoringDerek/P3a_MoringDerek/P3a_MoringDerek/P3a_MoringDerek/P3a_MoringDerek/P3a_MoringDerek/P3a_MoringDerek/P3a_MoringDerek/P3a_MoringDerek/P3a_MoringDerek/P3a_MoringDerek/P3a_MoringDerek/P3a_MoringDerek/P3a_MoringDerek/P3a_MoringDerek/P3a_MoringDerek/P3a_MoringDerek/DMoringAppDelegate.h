//
//  DMoringAppDelegate.h
//  P3a_MoringDerek
//
//  Created by Derek Moring on 7/24/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DMoringAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
