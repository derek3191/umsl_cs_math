//
//  Fraction.h
//  exc5
//
//  Created by Cezary Janikow on 3/20/14.
//  Copyright (c) 2014 Cezary Janikow. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Fraction : NSObject

@property (readonly) int num1;
@property (readonly) int denum1;
@property (readonly) int num2;
@property (readonly) int denum2;

-(id) init;
-(id) initWithNum1: (int) n1 andDenum1: (int)d1 andNum2: (int) n2 andDenum2: (int)d2;
+(Fraction*) sharedFraction;
+(Fraction*) addFractions;
+(Fraction*) multFractions;
-(NSString*) description;

@end
