//
//  main.m
//  P3a_MoringDerek
//
//  Created by Derek Moring on 7/24/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DMoringAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DMoringAppDelegate class]));
    }
}
