//
//  DMoringViewController.m
//  P3a_MoringDerek
//
//  Created by Derek Moring on 7/24/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "DMoringViewController.h"


@interface DMoringViewController ()
@property DMoringFraction *frac;
@end

@implementation DMoringViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *out = [NSString stringWithFormat:@"%i/%i",[[DMoringFraction sharedFraction]outN], [[DMoringFraction sharedFraction]outD]];
    self.resultLabel.text = out;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.num1TextField) {
        [[DMoringFraction sharedFraction]setNum1:self.num1TextField.text.intValue];
        [textField resignFirstResponder];
    }
    if (textField == self.num2TextField) {
        [[DMoringFraction sharedFraction]setNum2:self.num2TextField.text.intValue];
        [textField resignFirstResponder];
    }
    if (textField == self.denum1TextField) {
        [[DMoringFraction sharedFraction]setDenum1:self.denum1TextField.text.intValue];
        [textField resignFirstResponder];
    }
    if (textField == self.denum2TextField) {
        [[DMoringFraction sharedFraction]setDenum2:self.denum2TextField.text.intValue];
        [textField resignFirstResponder];
    }
    
    return YES;
}
- (IBAction)choosePressed:(id)sender {
    
}
@end
