//
//  ClassB.m
//  h5
//
//  Created by Derek Moring on 7/22/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "ClassB.h"

@implementation ClassB

-(id) init {
    self = [super init];
    if (self) {
        self.a1 = 1;
        self.a2 = @"Hello";
        self.a3 = 1;
        self.b = @"HelloB";
    }
    return self;
}
@end
