//
//  ClassB.h
//  h5
//
//  Created by Derek Moring on 7/22/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "ClassA.h"

@interface ClassB : ClassA

@property NSString *b;

@end
