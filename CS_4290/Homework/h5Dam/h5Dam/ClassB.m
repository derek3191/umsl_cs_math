//
//  ClassB.m
//  h5Dam
//
//  Created by Derek Moring on 7/23/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "ClassB.h"

@implementation ClassB

-(NSString *)description{
    NSString *out = [NSString stringWithFormat:@"\na1 is: %i\n a2 is: %@\n a3 is: %i\n b is: %@\n", self.a1, self.a2,self.a3, self.b];
    return out;
}

-(id) init{
    self = [super init];
    if (self) {
        self.a1 = 1;
        self.a2 = @"Hello";
        self.a3 = 1;
        self.b = @"3";
    }
    return self;
}

+(id) ClassBWithInit{
    return [[ClassB alloc] init];
}

@end
