//
//  ClassA.m
//  h5
//
//  Created by Derek Moring on 7/22/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//
@class fuck;

#import "ClassA.h"

@implementation ClassA

-(NSString *)description{
    NSString *out = [NSString stringWithFormat:@"\na1 is: %i\n a2 is: %@\n a3 is: %i\n", self.a1, self.a2,self.a3];
    return out;
}

-(id) init {
    self = [super init];
    if (self) {
        self.a1 = 1;
        self.a2 = @"Hello";
        self.a3 = 1;
    }
    return self;
}

+(id) ClassAWithInit{
    return [[ClassA alloc]init];
}

@end
