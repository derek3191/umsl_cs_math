//
//  ClassA.h
//  h5
//
//  Created by Derek Moring on 7/22/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ClassA : NSObject

@property int a1;
@property NSString *a2;
@property int a3;

-(NSString *)description;
-(id) init;
+(id) ClassAWithInit;

@end
