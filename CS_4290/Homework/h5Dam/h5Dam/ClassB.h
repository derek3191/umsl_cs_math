//
//  ClassB.h
//  h5Dam
//
//  Created by Derek Moring on 7/23/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "ClassA.h"

@interface ClassB : ClassA

@property NSString *b;

-(NSString *)description;
-(id) init;
+(id) ClassBWithInit;
@end
