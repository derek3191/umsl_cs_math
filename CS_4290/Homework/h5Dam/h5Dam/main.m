//
//  main.m
//  h5Dam
//
//  Created by Derek Moring on 7/23/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ClassB.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        ClassA *newA = [[ClassA alloc]init];
        ClassB *newB = [[ClassB alloc]init];
        
        NSLog(@"newA Description:\n----------------------\n");
        NSLog(@"%@",[newA description]);
        NSLog(@"newB Description:\n----------------------\n");
        NSLog(@"%@",[newB description]);
        
    }
    return 0;
}

