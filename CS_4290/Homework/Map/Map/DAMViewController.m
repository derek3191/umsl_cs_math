//
//  DAMViewController.m
//  Map
//
//  Created by Derek Moring on 7/16/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "DAMViewController.h"
#import "LocationManager.h"
@interface DAMViewController ()

@end

@implementation DAMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    //NSArray *zips = [[NSArray alloc] initWithObjects:@[@63052, @63048, @27455, @63028], nil];
    //NSLog(@"zips: %@", zips[0]);
    Location *a = [[Location alloc] initWithZip:63052 andLat:38.389552 andLon:-90.421944];
    Location *b = [[Location alloc] initWithZip:27455 andLat:36.185797 andLon:-79.818468];
    Location *c = [[Location alloc] initWithZip:63141 andLat:38.662494 andLon:-90.478549];
    Location *d = [[Location alloc] initWithZip:90001 andLat:33.969790 andLon:-118.246815];
    Location *e = [[Location alloc] initWithZip:77002 andLat:29.752554 andLon:-95.370401];
    Location *f = [[Location alloc] initWithZip:32801 andLat:28.541666 andLon:-81.375686];
    Location *g = [[Location alloc] initWithZip:63019 andLat:38.226315 andLon:-90.379544];
    Location *h = [[Location alloc] initWithZip:63048 andLat:38.257906 andLon:-90.398383];
    Location *i = [[Location alloc] initWithZip:63028 andLat:38.125935 andLon:-90.393672];
    Location *j = [[Location alloc] initWithZip:63129 andLat:38.453040 andLon:-90.308978];
                   
    
    NSArray *locArr = [[NSArray alloc] initWithObjects:a, b, c, d, e, f, g, h, i, j, nil];
    //int arr[10] = {63052, 27455, 63141, 90001, 77002,32801,63019,63048,63028,63129};
    //for (int i =0; i < 10;i++) {
    //    NSLog(@"zips: %i", arr[i]);
    //}
//    for (int i = 0; i < locArr.count; i++) {
//        NSLog(@"%@",[locArr objectAtIndex:i]);
//    }
    for (NSArray *it in locArr) {
        NSLog(@"%@", it);
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
