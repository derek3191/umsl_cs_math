//
//  Location.h
//  Map
//
//  Created by Derek Moring on 7/16/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Location : NSObject

@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *picFileName;
@property float latitude, longitude;
@property int zip;

-(Location *)initWithZip:(int)z andLat:(float)la andLon:(float)lo;

@end
