//
//  Location.m
//  Map
//
//  Created by Derek Moring on 7/16/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "Location.h"

@implementation Location
-(Location *)initWithZip:(int)z andLat:(float)la andLon:(float)lo{
    self = [super init];
    if (self) {
        self.latitude = la;
        self.longitude = lo;
        self.zip = z;
        self.address = @"";
        self.picFileName = @"";
    }
    return self;
}

@end
