//
//  LocationManager.h
//  Map
//
//  Created by Derek Moring on 7/16/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Location.h"
@interface LocationManager : NSObject

-(Location*) getLocation;
-(Location*) getLocationWithZip:(NSNumber *)zip;
-(void) storeZips;
@end
