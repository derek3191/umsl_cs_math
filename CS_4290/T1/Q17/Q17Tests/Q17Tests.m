//
//  Q17Tests.m
//  Q17Tests
//
//  Created by Derek Moring on 7/14/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Q17Tests : XCTestCase

@end

@implementation Q17Tests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
