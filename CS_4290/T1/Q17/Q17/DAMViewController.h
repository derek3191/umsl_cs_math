//
//  DAMViewController.h
//  Q17
//
//  Created by Derek Moring on 7/14/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DAMViewController : UIViewController<UITextFieldDelegate>

- (IBAction)pressedButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UILabel *display;

@end
