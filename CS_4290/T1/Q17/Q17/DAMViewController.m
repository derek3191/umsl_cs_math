//
//  DAMViewController.m
//  Q17
//
//  Created by Derek Moring on 7/14/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "DAMViewController.h"

@interface DAMViewController ()

@end

@implementation DAMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)pressedButton:(id)sender {
    NSMutableString *s;
    s = [NSMutableString stringWithString:@"Hello, "];
    [s appendString:self.nameTextField.text];
    
    self.display.text = s;

    if ([self.nameTextField isFirstResponder]) {
        [self.nameTextField resignFirstResponder];
    }
}

-(BOOL) textFieldShouldReturn:(UITextField *)
textField {
    if(textField == self.nameTextField)
        [textField resignFirstResponder];
    return YES;
}
@end
