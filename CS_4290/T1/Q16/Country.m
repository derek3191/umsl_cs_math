//
//  Country.m
//  Q16
//
//  Created by Derek Moring on 7/14/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import "Country.h"

@implementation Country

-(Country *) init {
    self = [super init];
    if (self) {
        self.name = @"United States of America";
        self.capital = @"Washington DC";
        self.population = 313900000;
    }
    return self;
}

-(Country *) initWithName: (NSString *)n andCapital: (NSString *)c andPopulation: (int) p {
    self = [super init];
    if (self) {
        self.name = n;
        self.capital = c;
        self.population = p;
    }
    return self;
}

@end
