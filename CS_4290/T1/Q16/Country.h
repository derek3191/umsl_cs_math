//
//  Country.h
//  Q16
//
//  Created by Derek Moring on 7/14/14.
//  Copyright (c) 2014 Damgfc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Country : NSObject

@property NSString *name;
@property NSString *capital;
@property int population;

-(Country *) init;
-(Country *) initWithName: (NSString *)n andCapital: (NSString *)c andPopulation: (int) p;
@end
