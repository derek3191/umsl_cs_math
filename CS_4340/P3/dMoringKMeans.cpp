#include <iostream>
#include <vector>
#include <cstdlib>
#include <cmath>

int numOfCoords = 50;
int intraDistance[3] = 0;
int centroidUpdates = 0;

typedef struct {
	double x;
	double y;
}coordinates;

typedef struct {
	int	centroidUpdates;
	int intraDistance;
	int totalX;
	int totalY;
	int totalCoords;
	vector<coordinates> coordinate;
	coordinates centroid;
} cluster;

using namespace std;

int main(int argc, char *argv[]) {
	vector<coordinates> coordPool;
	coordinates coord;
	cluster clusters[3];
	
	srand(time(NULL));
	
	// Create data points
	for (int i = 0; i < numOfCoords; i++) {
		coord.x = rand() % 11;
		coord.y = rand() % 11;
		coordPool.push_back(coord);
	}	
	
	// Generate random centroids
	for (int i = 0; i < 2; i++) {
		clusters[i].centroid.x = rand() % 11;
		clusters[i].centroid.y = rand() % 11;
	}
	
	// Find the closest centroid for each point
	int min = 50;
	double dist = 0;
	int cluster = 0;
	for (int i = 0; i < numOfCoords; i++) {
		for (int j = 0; j < 2; j++) {
			dist = getDistance(cluster[j].centroid, coordPool[i]);
			if (dist <= min){
				min = dist;
				cluster = j;
			}
		}
		clusters[cluster].coordinate.push_back(coordPool[i]); // move the coord to the cluster list of coords
		clusters.totalX++;
		clusters.totalY++;
		clusters.totalCoords++;
	}

	/***************************/
	/* Repeat above for k = 3; */
	/***************************/
	
	// Create data points
	for (int i = 0; i < numOfCoords; i++) {
		coord.x = rand() % 11;
		coord.y = rand() % 11;
		coordPool.push_back(coord);
	}	
	
	// Generate random centroids
	for (int i = 0; i < 3; i++) {
		clusters[i].centroid.x = rand() % 11;
		clusters[i].centroid.y = rand() % 11;
	}
	
	// Find the closest centroid for each point
	int min = 50;
	double dist = 0;
	int cluster = 0;
	for (int i = 0; i < numOfCoords; i++) {
		for (int j = 0; j < 3; j++) {
			dist = getDistance(cluster[j].centroid, coordPool[i]);
			if (dist <= min){
				min = dist;
				cluster = j;
			}
		}
		clusters[cluster].coordinate.push_back(coordPool[i]); // move the coord to the cluster list of coords
		clusters.totalX++;
		clusters.totalY++;
		clusters.totalCoords++;
	}

}

double getDistance(coordinate c, coordinate p){
	double a, b, r;
	a = pow(c.x - d.x, 2);
	b = pow(c.y - d.y, 2);
	r = sqrt(a + b);
	return r;
}

void addIntraDistance(int k, int dist){
	intraDistance[k] += dist;
}

