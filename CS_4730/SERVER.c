#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>

static int sockfd;
static void handle (int sig);

int main(int argc, char *argv[]) 
{
		struct sockaddr_in self;
		signal(SIGINT, handle);
	
		self.sin_family = AF_INET;
		self.sin_port = htons(3000);
	/*	self.sin_addr.s_addr = htonl (INADDR_ANY); */
		self.sin_addr.s_addr = inet_addr ("127.0.0.1");
		if (sockfd = socket (AF_INET, SOCK_STREAM, IPPROTO_TCP),  sockfd <0) {
			fprintf (stderr, "Socket open error\n");
			perror ("server"); exit (1);
			}
		if (bind (sockfd, (struct sockaddr *)(&self), sizeof (self)) < 0) {
			fprintf (stderr, "Socket bind error\n");
			perror ("server"); exit (1);
			}
		listen (sockfd, 2);

		for ( ; ; ) {
			char	string[50];
			int	newfd, client_len, act_len;
			struct sockaddr_in	client;

			newfd = accept (sockfd, (struct sockaddr *)(&client), &client_len);
			act_len = 0;
			do {
				act_len += read (newfd, &(string[act_len]), 50 - act_len);
			} while (string[act_len-1] != '\n');
			while (act_len > 0) {
				string[act_len] = '\000';
				reverse (string, act_len-1); 
				write (newfd, string, act_len);
				act_len = 0;
				do {
					act_len += read (newfd, &(string[act_len]), 50 - act_len);
				} while (string[act_len-1] != '\n');
				}
			close (newfd);
			}
	}

	int reverse (char *s, int len)
	{
		int i;
		for (i = 0; i < len / 2; i++) {
			char t = s[i];
			s[i] = s[len-1-i];
			s[len-1-i] = t;
			}
	}

	static void handle (int sig)
	{
		close (sockfd);
		exit (0);
	}

	dump (char *s, int l)
	{ int i;
		for (i = 0; i < l; i++)
			printf ("%x ", (unsigned char)s[i]);
		printf ("\n");
	}	
}