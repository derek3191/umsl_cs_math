#ifndef TOKEN_H
#define TOKEN_H

#include <string>
/*
typedef enum {
	FINALtk = -2,
	INVALIDtk = -1,
	EOFtk = 0,
	IDtk = 1,
	NUMtk = 2,
	EQUALEQUALtk = 3,
	LESSTHANtk = 4,
	GREATERTHANtk = 5,
	EQUALBANGEQUALtk = 6,//IS this still required???
	EQUALLESSTHANtk = 7,
	EQUALGREATERTHANtk = 8,
	EQUALtk = 9,
	COLONtk = 10,
	PLUStk = 11,
	MINUStk = 12,
	ASTERISKtk = 13,
	SLASHtk = 14,
	PERCENTtk = 15,
	PERIODtk = 16,
	LEFTPARENTHESIStk = 17,
	RIGHTPARENTHESIStk = 18,
	COMMAtk = 19,
	LEFTCURLYBRACKETtk = 20,
	RIGHTCURLYBRACKETtk = 21,
	SEMICOLONtk = 22,
	LEFTBRACKETtk = 23,
	RIGHTBRACKETtk = 24,
	STARTtk = 25,
	FINISHtk = 26,
	THENtk = 27,
	IFtk = 28,
	REPEATtk = 29,
	VARtk = 30,
	INTtk = 31,
	FLOATtk = 32,
	DOtk = 33,
	READtk = 34,
	PRINTtk = 35,
	VOIDtk = 36,
	RETURNtk = 37,
	DUMMYtk = 38,
	PROGRAMtk = 39
}tokenIDType;
*/
typedef struct {
	std::string tokenID;
	//tokenIDType tokenID;
	std::string tokenValue; //WHAT IS TOKEN INSTANCE???
	int lineNumber;
}tokenType;

tokenType tokenLookup(int returnValue, std::string instanceValue){
	tokenType token;
	typedef struct
	{
		int key;
		std::string tokenName;
		//tokenIDType tokenName;
	}tokenMap;

	tokenType keywordLookupTable[15] = {
		{"STARTtk", "start", 0},
		{"FINISHtk", "finish", 0},
		{"THENtk", "then", 0},
		{"IFtk", "if", 0},
		{"REPEATtk", "repeat", 0},
		{"VARtk", "var", 0},
		{"INTtk", "int", 0},
		{"FLOATtk", "float", 0},
		{"DOtk", "do", 0},
		{"READtk", "read", 0},
		{"PRINTtk", "print", 0},
		{"VOIDtk", "void", 0},
		{"RETURNtk", "return", 0},
		{"DUMMYtk", "dummy", 0},
		{"PROGRAMtk", "program", 0}
	};
/*
	tokenMap tokenLookupTable[25] = {
		{-99,EOFtk},
		{1,IDtk},
		{2,NUMtk},
		{3,EQUALEQUALtk},
		{4,LESSTHANtk},
		{5,GREATERTHANtk},
		{6,EQUALBANGEQUALtk},
		{7,EQUALLESSTHANtk},
		{8,EQUALGREATERTHANtk},
		{9,EQUALtk},
		{10,COLONtk},
		{11,PLUStk},
		{12,MINUStk},
		{13,ASTERISKtk},
		{14,SLASHtk},
		{15,PERCENTtk},
		{16,PERIODtk},
		{17,LEFTPARENTHESIStk},
		{18,RIGHTPARENTHESIStk},
		{19,COMMAtk},
		{20,LEFTCURLYBRACKETtk},
		{21,RIGHTCURLYBRACKETtk},
		{22,SEMICOLONtk},
		{23,LEFTBRACKETtk},
		{24,RIGHTBRACKETtk}
	};
*/
	tokenMap tokenLookupTable[25] =
	{
		{-99,"EOFtk"},
		{1,"IDtk"},
		{2,"NUMtk"},
		{3,"EQUALEQUALtk"},
		{4,"LESSTHANtk"},
		{5,"GREATERTHANtk"},
		{6,"EQUALBANGEQUALtk"},
		{7,"EQUALLESSTHANtk"},
		{8,"EQUALGREATERTHANtk"},
		{9,"EQUALtk"},
		{10,"COLONtk"},
		{11,"PLUStk"},
		{12,"MINUStk"},
		{13,"ASTERISKtk"},
		{14,"SLASHtk"},
		{15,"PERCENTtk"},
		{16,"PERIODtk"},
		{17,"LEFTPARENTHESIStk"},
		{18,"RIGHTPARENTHESIStk"},
		{19,"COMMAtk"},
		{20,"LEFTCURLYBRACKETtk"},
		{21,"RIGHTCURLYBRACKETtk"},
		{22,"SEMICOLONtk"},
		{23,"LEFTBRACKETtk"},
		{24,"RIGHTBRACKETtk"}
	};
	for (int i = 0; i < 25; ++i) //size of array tokenLookupTable
	{
		if (returnValue == 1)
		{
			for (int j = 0; j < 15; j++)
			{
				if (keywordLookupTable[j].tokenValue == instanceValue){
					token.tokenID = keywordLookupTable[j].tokenID;
					token.tokenValue = instanceValue;
					token.lineNumber = 7;
				}
			}
		}
		if (tokenLookupTable[i].key == returnValue)
		{
			token.tokenID = tokenLookupTable[i].tokenName;
			token.tokenValue = instanceValue;
			token.lineNumber = 6;
		}
	}
	return token;
}


#endif