//scanner.cpp 

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include "stateTable.h"
using namespace std;

int lookupColumn(char currentLookaheadChar){
	typedef struct alphabetKey
	{
		int key;
		int value;
	} alphabetKey;

	alphabetKey charLookup[19] = {
		{'=',	0},
		{'<',	1},
		{'>',	2},
		{':',	3},
		{'+',	4},
		{'-',	5},
		{'*',	6},
		{'/',	7},
		{'%',	8},
		{'!',	9},
		{'.',	10},
		{'(',	11},
		{')',	12},
		{',',	13},
		{'{',	14},
		{'}',	15},
		{';',	16},
		{'[',	17},
		{']',	18},
	};

for (int i = 0; i < 19; i++)
{
	if (charLookup[i].key == currentLookaheadChar)
	{	
		return charLookup[i].value;
	}
}

if (isalpha(currentLookaheadChar))
{
	return 19;
}
else if (isdigit(currentLookaheadChar))
{
	return 20;
}
else if (isspace(currentLookaheadChar))
{
	return 21;
}
else if (currentLookaheadChar == EOF) 
{
	return 22;
}
else
	return 23;
}

void getFile(FILE *fp){
	int lookahead;
	tokenType token;
	int stateTableColumn;
	int currentState, nextState = 0;
	string stringInstance;
	int lineNumber = 1;

	while (currentState != -2){
		lookahead = fgetc(fp);
		if (lookahead == '#') {
			while (lookahead != '\n'){
				lookahead = fgetc(fp);
			}
			lineNumber++;
		}
		else {
			stateTableColumn = lookupColumn(lookahead);
			nextState = stateTableValues[currentState][stateTableColumn];


			if (nextState < 0)
			{
				if (lookahead == '\n')
				{
					lineNumber++;
				}
				if (nextState == -99)
				{
					ungetc(lookahead, fp);
					currentState = 0;
					cout << "EOF has been met\n";
					break;break;
				}
				else if (nextState == -2){
					for (int i = 0; i < stringInstance.size(); ++i)
					{
						if (!isspace(stringInstance[i]))
						{
							stringInstance[i] = tolower(stringInstance[i]);
						}
					}
					token = tokenLookup(currentState, stringInstance);


					cout << "Value: " << token.tokenValue << "\tToken: " << 
					setw(18) <<	token.tokenID << "\tLineNumber: " << lineNumber << endl;

					currentState = nextState = 0;
					ungetc(lookahead, fp);
					stringInstance.erase(stringInstance.begin(), stringInstance.end());

				}
				else {//if (nextState == -1){
					cout << "error at Line: " << lineNumber << " with value: " <<
					static_cast<char> (lookahead) << endl;
					return;
				}
				
			}
			else {
				char characterValue = static_cast<char> (lookahead);

				if (!isspace(characterValue))
				{
					stringInstance += tolower(characterValue);
				}
				if (nextState == 6)
				{
					char look2 = fgetc(fp);

					if (look2 == '=')	
					{
						stringInstance += static_cast<char> (look2); 
					}
					else {
						stringInstance += static_cast<char> (look2);
						ungetc(look2, fp);
						//return;
					}
				}
				currentState = nextState;
			}
		}
	}
}