/**************************/
/*  Derek Moring CS4280   */
/*        Project 0       */ 
/*     traversals.cpp     */
/**************************/
#include "traversals.h"
#include <iostream>

int currentLevel = 0;
void inOrder(node *root){
	if(root != NULL){
		inOrder(root->left);
		inOrder(root->middle);
		for (int i = 0; i < root->level; ++i) {
			cout << " ";
		}
		cout << root->keyLetter << ":\t";
		for (vector<string>::size_type i = 0; i < root->wordsInNodeVector.size(); i++){
			cout << root->wordsInNodeVector[i] << " ";
		}
		cout << endl;
		inOrder(root->right);
	}
}

void preOrder(node *root){
	if(root != NULL){
		for (int i = 0; i < root->level; ++i){
			cout << " ";
		}
		cout << root->keyLetter << ":\t";
		for (vector<string>::size_type i = 0; i < root->wordsInNodeVector.size(); i++){
			cout << root->wordsInNodeVector[i] << " ";
		}
		cout << endl;
		inOrder(root->left);
		inOrder(root->middle);
		inOrder(root->right);
	}
}

void postOrder(node *root){
	if(root != NULL){
		inOrder(root->left);
		inOrder(root->middle);
		inOrder(root->right);
		for (int i = 0; i < root->level; ++i) {
			cout << " ";
		}
		cout << root->keyLetter << ":\t";
		for (vector<string>::size_type i = 0; i < root->wordsInNodeVector.size(); i++){
			cout << root->wordsInNodeVector[i] << " ";
		}
		cout << endl;
	}
}

