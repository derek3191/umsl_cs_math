/**************************/
/*  Derek Moring CS4280   */
/*        Project 0       */ 
/*      buildTree.cpp     */
/**************************/
#include "buildTree.h"
#include "node.h"
#include <iostream>
#include <string>

using namespace std;
node *root = NULL;

node *createLeaf(char keyLetter, string word, int level){
	node *n = new node;
	n->left = NULL;
	n->middle = NULL;
	n->right = NULL;
	n->parent = NULL;
	n->keyLetter = keyLetter;
	n->wordsInNodeVector.push_back(word);
	n->level = level;
	return n;	
}

void addLeaf(char keyLetter, string word){
	addLeafPrivate(keyLetter, word, root);
}

void addLeafPrivate(char keyLetter, string word, node *ptr){
	if (root == NULL){
		root = createLeaf(keyLetter, word, 0);
		root->parent = root;
	}
	else if (keyLetter < ptr->keyLetter){
		if (ptr->left == NULL){
			ptr->left = createLeaf(keyLetter, word, ((ptr->level)+1));
			ptr->left->parent = ptr;
		}
		else {
			if (ptr->middle == NULL){
				if (keyLetter == ptr->left->keyLetter){
					if(checkForMatches(word, ptr->left) != 0){
						ptr->left->wordsInNodeVector.push_back(word);
					}
				}
				else {
					ptr->middle = createLeaf(keyLetter, word, ((ptr->level)+1));
					ptr->middle->parent = ptr;
					if (ptr->left->keyLetter > ptr->middle->keyLetter){
						node *tempNode = ptr->left;
						ptr->left = ptr->middle;
						ptr->middle = tempNode;
					}
				}	
			}
			else {
				if (keyLetter < ptr->middle->keyLetter){
					if (keyLetter == ptr->left->keyLetter){
						if (checkForMatches(word, ptr->left) !=0 )						
							ptr->left->wordsInNodeVector.push_back(word);
					}
					else {
					 	addLeafPrivate(keyLetter, word, ptr->left);
					}
				}
				else if (keyLetter == ptr->middle->keyLetter){
					if (checkForMatches(word, ptr->middle) !=0 ){
						ptr->middle->wordsInNodeVector.push_back(word);
					}
				}
				else 
					addLeafPrivate(keyLetter, word, ptr->middle);
			}
		}
	}
	else if (keyLetter == ptr->keyLetter) {
		if (checkForMatches(word, ptr) != 0 )
			ptr->wordsInNodeVector.push_back(word);
	}
	else {
		if(ptr->right == NULL){
			ptr->right = createLeaf(keyLetter, word, ((ptr->level)+1));
			ptr->right->parent = ptr;
		}
		else if (keyLetter == ptr->right->keyLetter){
			if (checkForMatches(word, ptr->right) != 0)
				ptr->right->wordsInNodeVector.push_back(word);
		}
		else
			addLeafPrivate(keyLetter, word, ptr->right);
	}
}

int checkForMatches(string word, node *ptr){
	if (ptr->wordsInNodeVector.size() > 0 ){
		for (int i = 0; i < ptr->wordsInNodeVector.size(); i++){
			if (ptr->wordsInNodeVector[i].compare(word) == 0){
				return 0;
			}
			else 
				return 1;
		}
	}
	return 0;
}

node *getRoot(){
	return root; 
}

