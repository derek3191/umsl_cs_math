/**************************/
/*  Derek Moring CS4280   */
/*      Project 0         */ 
/*     traversals.h       */
/**************************/
#include "node.h"
void inOrder(node *n);
void preOrder(node *n);
void postOrder(node *n);
