/**************************/
/*  Derek Moring CS4280   */
/*       Project 0        */ 
/*      buildTree.h       */
/**************************/
#include "node.h"

node *createLeaf(node keyLetter, string word, int level);
void addLeaf(char keyLetter, string word);
void addLeafPrivate(char keyLetter, string word, node *ptr);
int checkForMatches(string word, node *ptr);
node *getRoot();
